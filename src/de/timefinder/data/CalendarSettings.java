/*
 *  Copyright 2009 Peter Karich, peat_hal ‘at’ users ‘dot’ sourceforge ‘dot’ net.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */
package de.timefinder.data;

import de.timefinder.data.util.DataPoolSettingsHelper;
import java.beans.PropertyChangeListener;
import java.io.File;
import org.joda.time.DateTime;

/**
 * @author Peter Karich, peat_hal ‘at’ users ‘dot’ sourceforge ‘dot’ net
 */
public class CalendarSettings implements ICalendarSettings {

    private DateTime startDate = new DateTime(2009, 4, 6, 8, 0, 0, 0);
    private long milliSecondsPerTimeslot = 60 * 60 * 1000L;
    private int timeslotsPerDay = 12;
    private int numberOfDays = 7;
    private DataPoolSettingsHelper helper;

    public DateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(DateTime start) {
        startDate = start;
    }

    public long getMillisPerTimeslot() {
        return milliSecondsPerTimeslot;
    }

    public void setMillisPerTimeslot(long milliSecondsPerTimeslot) {
        this.milliSecondsPerTimeslot = milliSecondsPerTimeslot;
    }

    public int getTimeslotsPerWeek() {
        return getNumberOfDays() * getTimeslotsPerDay();
    }

    public void setTimeslotsPerDay(int timeslotsPerDay) {
        this.timeslotsPerDay = timeslotsPerDay;
    }

    public int getTimeslotsPerDay() {
        return timeslotsPerDay;
    }

    public int getNumberOfDays() {
        return numberOfDays;
    }

    public void setNumberOfDays(int numberOfDays) {
        this.numberOfDays = numberOfDays;
    }

    public boolean addListener(PropertyChangeListener l) {
        return getHelper().addListener(l);
    }

    public boolean removeListener(PropertyChangeListener l) {
        return getHelper().removeListener(l);
    }

    public void fireChanges() {
        getHelper().fireChanges();
    }

    private DataPoolSettingsHelper getHelper() {
        if (helper == null) {
            helper = new DataPoolSettingsHelper(this, null);
        }
        return helper;
    }

    @Override
    public DateTime toDateTime(int timeslot) {
        return getHelper().toDateTime(timeslot);
    }

    @Override
    public IntervalInt toInterval(DateTime start, DateTime end) {
        return getHelper().toEvent(start, end);
    }

    @Override
    public IntervalLong toIntervalLong(IntervalInt simpleInt) {
        return getHelper().toInterval(simpleInt);
    }

    @Override
    public Task toTask(IntervalInt simpleInt) {
        return getHelper().toTask(simpleInt);
    }

    @Override
    public Object clone() {
        try {
            CalendarSettings set = (CalendarSettings) super.clone();
            set.helper = new DataPoolSettingsHelper(set, set.getHelper());
            return set;
        } catch (Exception e) {
            throw new UnsupportedOperationException("cloning failed", e);
        }
    }
}
