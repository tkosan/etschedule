/*
 *  Copyright 2009 Peter Karich, peat_hal ‘at’ users ‘dot’ sourceforge ‘dot’ net.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */
package de.timefinder.data;

import de.timefinder.data.algo.Constraint;
import java.util.Collection;
import javolution.util.FastMap;
import java.util.Map;

/**
 * This object is yet another super class of all entities,
 * because
 * 1. we want to separate the database and the constraint methods
 * 2. we don't want to repeat this constraint stuff for Person, Event and Location
 * 3. java does not support multiple inheritance or mixins
 *
 * @author Peter Karich, peat_hal ‘at’ users ‘dot’ sourceforge ‘dot’ net
 */
public class ConstraintObject extends DBImpl {

    private static final long serialVersionUID = -818419940539992628L;
    private Map<Class<? extends Constraint>, Constraint> constraints;

    private Map<Class<? extends Constraint>, Constraint> getConstraintsMap() {
        if (constraints == null) {
            constraints = new FastMap<Class<? extends Constraint>, Constraint>();
        }
        return constraints;
    }

    public Collection<? extends Constraint> getConstraints() {
        return getConstraintsMap().values();
    }

    public void setConstraints(Collection<? extends Constraint> coll) {
        getConstraintsMap().clear();
        if (coll != null)
            for (Constraint c : coll) {
                if (c == null)
                    throw new NullPointerException("Shouldn't add null elements!");
                getConstraintsMap().put(c.getClass(), c);
            }
    }

    @SuppressWarnings("unchecked")
    public <C extends Constraint> C getConstraint(Class<C> clazz) {
        return (C) getConstraintsMap().get(clazz);
    }

    public void putConstraint(Constraint c) {
        getConstraintsMap().put(c.getClass(), c);
    }

    public boolean removeConstraint(Constraint c) {
        return getConstraintsMap().remove(c.getClass()) != null;
    }
}
