/*
 *  Copyright 2009 Peter Karich.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */
package de.timefinder.data;

import java.io.Serializable;

/**
 * @author Peter Karich, peat_hal ‘at’ users ‘dot’ sourceforge ‘dot’ net
 */
public class DBImpl implements DBInterface, Serializable {

    private static final long serialVersionUID = -286507607306610589L;    
    private Long id;    
    private String name;

    public DBImpl() {
    }

    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return null if not attached to any datasource (e.g. via hibernate)
     */
    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String str) {
        name = str;
    }

    public boolean equalsProperties(Object obj) {
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final DBInterface other = (DBInterface) obj;
        if (this.name != other.getName() && (this.name == null || !this.name.equals(other.getName()))) {
            return false;
        }
        return true;
    }

    // WARNING
    // We can have 3 different, but useful equalities
    //
    // 1. based on id (same objects from database ... )
    // 2. based on equal reference  (same objects in memory ... )
    // 3. based on equal properties (semantic the same objects ... )
    @Override
    public String toString() {
        return "id:" + id + ", name:" + name;
    }
}
