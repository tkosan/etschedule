/*
 * This file is part of the TimeFinder project.
 * Visit http://www.timefinder.de for more information.
 * Copyright 2008 the original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.timefinder.data;

/**
 * The base class for all objects which should be 'storable.
 * Via the data-access-object pattern this can be a database or a filebased
 * storage mechanism.
 *
 * @see http://java.sun.com/blueprints/corej2eepatterns/Patterns/DataAccessObject.html
 * @author Peter Karich, peat_hal ‘at’ users ‘dot’ sourceforge ‘dot’ net
 */
public interface DBInterface {

    void setId(Long id);

    /**
     * @return null if not attached to any datasource (e.g. via hibernate)
     */
    Long getId();

    String getName();

    void setName(String str);

    boolean equalsProperties(Object obj);
    
    // WARNING
    // We can have 3 different, but useful equalities
    //
    // 1. based on id (same objects from database ... )
    // 2. based on equal reference  (same objects in memory ... )
    // 3. based on equal properties (same 'semantic' objects ... )
}
