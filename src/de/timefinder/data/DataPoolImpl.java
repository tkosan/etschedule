/*
 *  Copyright 2009 Peter Karich, peat_hal ‘at’ users ‘dot’ sourceforge ‘dot’ net.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */
package de.timefinder.data;

import de.timefinder.data.access.Dao;
import java.util.ArrayList;
import java.util.Collections;
import javolution.util.FastMap;
import java.util.List;
import java.util.Map;

/**
 * The simplest implementation of DataPool
 * 
 * @author Peter Karich, peat_hal ‘at’ users ‘dot’ sourceforge ‘dot’ net
 */
public class DataPoolImpl implements DataPool {

    private Map<Class, Dao<? extends DBInterface>> map;

    public DataPoolImpl() {
        map = new FastMap<Class, Dao<? extends DBInterface>>();
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T extends DBInterface> Dao<T> getDao(Class<T> clazz) {
        return (Dao<T>) map.get(clazz);
    }

    @Override
    public List<Dao<? extends DBInterface>> getDaos() {
        return Collections.unmodifiableList(new ArrayList(map.values()));
    }

    @Override
    public void setDaos(List<Dao<? extends DBInterface>> daos) {
        for (Dao<? extends DBInterface> dao : daos) {
            setDao(dao);
        }
    }

    @Override
    public void setDao(Dao<? extends DBInterface> dao) {
        if (map.put(dao.getType(), dao) != null) {
            throw new IllegalStateException("Overwriting DAO of type " + dao.getType() +
                    " with size of " + dao.getAll().size() + " not allowed");
        }
    }

    public void clear() {
        map.clear();
    }
}
