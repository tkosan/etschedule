/*
 * This file is part of the TimeFinder project.
 *  Visit http://www.timefinder.de for more information.
 *  Copyright (c) 2009 the original author or authors.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package de.timefinder.data;

import javolution.util.FastMap;
import javolution.util.FastSet;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

/**
 * An event is a distinct time-interval of one Periode.
 * The start variable is relative to the beginning of one Periode and the
 * duration variable's unit is in timeslots.
 *
 * @author Peter Karich, peat_hal ‘at’ users ‘dot’ sourceforge ‘dot’ net
 */
public class Event extends ConstraintObject implements FeatureContainer, IntervalInt {

    private static final long serialVersionUID = 3384959364539450289L;
    private String description;
    private int start;
    private int duration;
    private Location location;
    private Map<Person, Role> persons;
    private Set<Feature> features;

    public Event() {
        duration = 1;
    }

    public Event(int start, int duration) {
        setStart(start);
        setDuration(duration);
    }

    public void setInterval(int start, int duration) {
        setStart(start);
        setDuration(duration);
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getEnd() {
        return start + duration;
    }

    public void setDuration(int duration) {
        if (duration <= 0) {
            throw new UnsupportedOperationException("duration has to be positive");
        }
        this.duration = duration;
    }

    public int getDuration() {
        return duration;
    }

    public boolean overlapps(IntervalInt interval) {
        return (start >= interval.getStart() && start < interval.getEnd())
                || (interval.getStart() >= start && interval.getStart() < getEnd());
    }

    public static boolean overlapps(IntervalInt interval1, int start1, IntervalInt interval2, int start2) {
        return (start1 >= start2 && start1 < interval2.getEnd())
                || (start2 >= start1 && start2 < interval1.getEnd());
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location loc) {
        setLocation(loc, true);
    }

    public void setLocation(Location loc, boolean reverse) {
        Location oldLocation = location;
        location = loc;
        if (location == null) {
            if (oldLocation != null && reverse)
                oldLocation.removeEvent(this, false);
        } else if (reverse) {
            location.addEvent(this, false);
        }
    }

    public Map<Person, Role> getPersonsMap() {
        if (persons == null) {
            persons = new FastMap<Person, Role>();
        }
        return persons;
    }

    public Collection<Person> getPersons() {
        return getPersonsMap().keySet();
    }

    public int getPersonsSize() {
        return getPersonsMap().size();
    }

    public void setPersons(Collection<Person> coll) {
        getPersonsMap().clear();
        if (coll != null) {
            for (Person p : coll) {
                addPerson(p, true);
            }
        }
    }

    public void addPerson(Person p, boolean reverseAdd) {
        addPerson(p, Role.STUDENT, reverseAdd);
    }

    public void addPerson(Person p, Role role, boolean reverseAdd) {
        getPersonsMap().put(p, role);
        if (reverseAdd) {
            p.addEvent(this, false);
        }
    }

    public boolean removePerson(Person p, boolean reverseRemove) {
        boolean ret = getPersonsMap().remove(p) != null;
        if (ret) {
            if (reverseRemove) {
                p.removeEvent(this, false);
            }
        }
        return ret;
    }

    @Override
    public Collection<Feature> getFeatures() {
        if (features == null) {
            features = new FastSet<Feature>();
        }
        return features;
    }

    @Override
    public void addFeature(Feature feature) {
        getFeatures().add(feature);
    }

    public void setFeatures(Collection<Feature> coll) {
        getFeatures().clear();
        if (coll != null) {
            for (Feature f : coll) {
                addFeature(f);
            }
        }
    }

    /**
     * This method removes one feature from the event.
     */
    @Override
    public boolean removeFeature(Feature feature) {
        boolean ret = getFeatures().remove(feature);
        return ret;
    }

    @Override
    public boolean equalsProperties(Object obj) {
        if (obj == null || getClass() != obj.getClass()
                || !super.equalsProperties(obj)) {
            return false;
        }

        final Event other = (Event) obj;
        if (this.start != other.start) {
            return false;
        }

        if (this.duration != other.duration) {
            return false;
        }

        if (this.location != other.location
                && (this.location == null || !this.location.equals(other.location))) {
            return false;
        }

        if (this.persons != other.persons
                && (this.persons == null || !this.persons.equals(other.persons))) {
            return false;
        }

        return true;
    }

    @Override
    public String toString() {
        return "Event[start:" + start + ", duration:" + duration + ", " + super.toString() + "]";
    }

    @Override
    public String getDetails() {
        return getName() + (getLocation() == null ? "" : "\nlocation:" + getLocation().getName());
    }
}
