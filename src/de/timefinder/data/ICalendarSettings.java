/*
 *  Copyright 2009 Peter Karich, peat_hal ‘at’ users ‘dot’ sourceforge ‘dot’ net.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */
package de.timefinder.data;

import java.beans.PropertyChangeListener;
import org.joda.time.DateTime;

/**
 * @author Peter Karich, peat_hal ‘at’ users ‘dot’ sourceforge ‘dot’ net
 */
public interface ICalendarSettings extends Cloneable {

    DateTime getStartDate();

    void setStartDate(DateTime start);

    long getMillisPerTimeslot();

    void setMillisPerTimeslot(long milliSecondsPerTimeslot);

    int getTimeslotsPerWeek();

    /**
     * Specifies how many timeslots we have for one day.
     */
    int getTimeslotsPerDay();

    void setTimeslotsPerDay(int timeslotsPerDay);

    int getNumberOfDays();

    void setNumberOfDays(int numberOfDays);

    boolean addListener(PropertyChangeListener l);

    boolean removeListener(PropertyChangeListener l);

    void fireChanges();

    IntervalLong toIntervalLong(IntervalInt simpleInt);

    Task toTask(IntervalInt simpleInt);

    DateTime toDateTime(int timeslot);

    /**
     * @return a new IntervalInt where this settings will be applied. Returns
     * null if not possible.
     */
    IntervalInt toInterval(DateTime start, DateTime end);

    Object clone();
    /**
     * This property name indicates on PropertyChangeEvent that all settings
     * could have been changed.
     */
    public static final String CHANGE_ALL = "all";
    /**
     * millis that are one minute.
     */
    public static final long MINUTE = 60 * 1000L;
    /**
     * millis that are one hour.
     */
    public static final long HOUR = 60 * MINUTE;
    /**
     * millis that are one day.
     */
    public static final long DAY = 24 * HOUR;
}
