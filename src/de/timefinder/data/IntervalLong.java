/*
 *  Copyright 2009 Peter Karich.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */
package de.timefinder.data;

import java.awt.Color;
import org.joda.time.DateTime;

/**
 * This interface is designed simply so that it can be used in different
 * places.
 *
 * @see IntervalInt
 * @author Peter Karich
 */
public interface IntervalLong {

    long getStart();

    void setStart(long start);

    void setEnd(long end);

    long getEnd();

    DateTime getStartDateTime();

    DateTime getEndDateTime();

    void setDuration(long duration);

    long getDuration();

    void setInterval(long start, long duration);

    void setAssigned(boolean ass);

    boolean isAssigned();

    boolean overlapps(IntervalLong se);

    String getName();

    void setName(String str);

    String getDescription();

    void setDescription(String text);
    
    void setColor(Color color);
    
    Color getColor();
    
    void setIsBorder(boolean isBorder);
    
    boolean isBorder();
    
    int getThickness();
    
    void setThickness(int thickness);
    
    int getNumberOfConflicts();
    
    void setNumberOfConflicts(int numberOfConflicts);
}
