/*
 *  Copyright 2009 Peter Karich.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */
package de.timefinder.data;

import java.awt.Color;
import org.joda.time.DateTime;

/**
 * @author Peter Karich
 */
public class IntervalLongImpl implements IntervalLong {

    private long start;
    private long duration;
    private boolean assigned;
    private String name;
    private String description;
    private Color color;
    private boolean isBorder;
    private int thickness;
    private int numberOfConflicts;

    public IntervalLongImpl() {
    }

    public IntervalLongImpl(DateTime start, int durationInMinutes) {
        this(start.getYear(), start.getMonthOfYear(), start.getDayOfMonth(),
                start.getHourOfDay(), start.getMinuteOfHour(), durationInMinutes);
    }

    public IntervalLongImpl(String description, int year, int month, int day, int hour, int minute, int durationInMinutes) {
        this(year, month, day, hour, minute, durationInMinutes);
        setDescription(description);
    }

    public IntervalLongImpl(int year, int month, int day, int hour, int minute, int durationInMinutes) {
        start = new DateTime(year, month, day, hour, minute, 0, 0).getMillis();
        setInterval(start, durationInMinutes * 60 * 1000);
    }

    public IntervalLongImpl(long start, long duration) {
        this.start = start;
        this.duration = duration;
    }

    public void setInterval(long start, long duration) {
        setStart(start);
        setDuration(duration);
    }

    public long getStart() {
        return start;
    }

    public void setStartDateTime(DateTime startDT) {
        setStart(startDT.getMillis());
    }

    public void setEndDateTime(DateTime endDT) {
        setDuration(endDT.getMillis() - start);
    }

    public void setStart(long start) {
        if (start < 0) {
            throw new UnsupportedOperationException("To unassign the start time use setAssigned(false).");
        }

        this.start = start;
    }

    public void setEnd(long end) {
        setStart(end - duration);
    }

    public long getEnd() {
        return start + duration;
    }

    public void setDuration(long duration) {
        if (duration <= 0) {
            throw new UnsupportedOperationException("duration has to be positive");
        }
        this.duration = duration;
    }

    public long getDuration() {
        return duration;
    }

    public boolean overlapps(IntervalLong interval) {
        return (start >= interval.getStart() && start < interval.getEnd()) ||
                (interval.getStart() >= start && interval.getStart() < getEnd());
    }

    public void setAssigned(boolean ass) {
        assigned = ass;
    }

    public boolean isAssigned() {
        return assigned;
    }

    public DateTime getStartDateTime() {
        return new DateTime(start);
    }

    public DateTime getEndDateTime() {
        return new DateTime(start + duration);
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String text) {
        this.description = text;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String str) {
        name = str;
    }

    @Override
    public Color getColor() {
        return color;
    }

    @Override
    public void setColor(Color color) {
        this.color = color;
    }

    @Override
    public boolean isBorder() {
        return isBorder;
    }
    
    @Override
    public void setIsBorder(boolean isBorder) {
        this.isBorder = isBorder;
    }

    @Override
    public int getThickness() {
        return thickness;
    }

    @Override
    public void setThickness(int thickness) {
        this.thickness = thickness;
    }

    @Override
    public int getNumberOfConflicts() {
        return numberOfConflicts;
    }

    @Override
    public void setNumberOfConflicts(int numberOfConflicts) {
        this.numberOfConflicts = numberOfConflicts;
    }
    
    
    
    @Override
    public String toString() {
        return getDescription() == null ? getName() : getDescription() + " from:" + getStartDateTime() + " to:" + getEndDateTime();
    }
}
