/*
 * This file is part of the TimeFinder project.
 *  Visit http://www.timefinder.de for more information.
 *  Copyright (c) 2009 the original author or authors.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package de.timefinder.data;

import javolution.util.FastSet;

import java.util.Collection;
import java.util.Set;

/**
 * This entity characterizes a place or a room with some sort of capacity.
 * E.g. seats or space. Every location could offer special equipment or
 * characterizations - I called them feature. An event could then require
 * one or more features
 *
 * @author Peter Karich, peat_hal ‘at’ users ‘dot’ sourceforge ‘dot’ net
 */
public class Location extends ConstraintObject implements FeatureContainer, Resource {

    private static final long serialVersionUID = -428522737279489854L;
    private int capacity;
    private Set<Event> events;
    private Set<Feature> features;

    public Location() {
    }

    public Location(int capacity) {
        setCapacity(capacity);
    }

    public Location(String name, int capacity) {
        setName(name);
        setCapacity(capacity);
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int cap) {
        capacity = cap;
    }

    public int getEventsSize() {
        return getEvents().size();
    }

    @Override
    public Collection<Event> getEvents() {
        if (events == null)
            events = new FastSet<Event>();

        return events;
    }

    /**
     * This method does not set the backward reference
     */
    public void setEvents(Collection<Event> events) {
        getEvents().clear();
        if (events != null)
            for (Event ev : events) {
                addEvent(ev, true);
            }
    }

    public void addEvent(Event ev, boolean reverseAdd) {
        boolean ret = getEvents().add(ev);
        if (ret && reverseAdd)
            ev.setLocation(this, false);
    }

    public boolean removeEvent(Event ev, boolean reverseRemove) {
        boolean ret = getEvents().remove(ev);
        if (ret && reverseRemove)
            ev.setLocation(null, false);

        return ret;
    }

    public void setFeatures(Collection<Feature> coll) {
        getFeatures().clear();
        if (coll != null) {
            for (Feature f : coll) {
                addFeature(f);
            }
        }
    }

    @Override
    public Collection<Feature> getFeatures() {
        if (features == null)
            features = new FastSet<Feature>();
        return features;
    }

    /**
     * This method add one feature which is offered from the location.
     */
    @Override
    public void addFeature(Feature feature) {
        getFeatures().add(feature);
    }

    /**
     * This method removes one feature from the location.
     */
    @Override
    public boolean removeFeature(Feature feature) {
        return getFeatures().remove(feature);
    }

    @Override
    public String toString() {
        return "Location[capacity:" + capacity + ", " + super.toString() + "]";
    }
}
