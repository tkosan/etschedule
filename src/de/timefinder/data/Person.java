/*
 * This file is part of the TimeFinder project.
 * Visit http://www.timefinder.de for more information.
 * Copyright 2008 the original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.timefinder.data;

import java.util.Collection;
import java.util.Date;
import javolution.util.FastMap;
import java.util.Map;
import java.util.Map.Entry;

/**
 * This class represents the person entity, which has in the most cases
 * a real counterpart.
 *
 * @author Peter Karich, peat_hal ‘at’ users ‘dot’ sourceforge ‘dot’ net
 */
public class Person extends ConstraintObject implements Resource {

    private static final long serialVersionUID = 5075054837825796151L;
    private String firstName;
    private String description;
    private String state;
    private String city;
    private String street;
    private String streetNumber;
    private String zip;
    private String telephon;
    private String mobil;
    private Date birthDay;
    private Map<Event, Role> events;

    public Person() {
    }

    public Person(String name) {
        setName(name);
    }

    public void setLastName(String lastName) {
        setName(lastName);
    }

    public String getLastName() {
        return getName();
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMobil() {
        return mobil;
    }

    public void setMobil(String mobil) {
        this.mobil = mobil;
    }

    public String getTelephon() {
        return telephon;
    }

    public void setTelephon(String telephon) {
        this.telephon = telephon;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getStreetNumber() {
        return streetNumber;
    }

    public void setStreetNumber(String streetNumber) {
        this.streetNumber = streetNumber;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public Date getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(Date birthDay) {
        this.birthDay = birthDay;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Role getRole(Event ev) {
        Role role = getEventsMap().get(ev);
        if (role == null) {
            role = Role.STUDENT;
            getEventsMap().put(ev, role);
        }
        return role;
    }

    public Map<Event, Role> getEventsMap() {
        if (events == null) {
            events = new FastMap<Event, Role>();
        }
        return events;
    }

    @Override
    public Collection<Event> getEvents() {
        return getEventsMap().keySet();
    }

    public int getEventsSize() {
        return getEventsMap().size();
    }

    public void removeAllEvents() {
        getEventsMap().clear();
    }

    public void setEventsMap(Map<Event, Role> map) {
        removeAllEvents();
        if (map != null) {
            for (Entry<Event, Role> entry : map.entrySet()) {
                addEvent(entry.getKey(), entry.getValue(), true);
            }
        }
    }

    /**
     * This method adds the event to the person.
     * @param reverseAdd if true the person will be added to the event as well.
     */
    public void addEvent(Event ev, Role r, boolean reverseAdd) {
        boolean ret = getEventsMap().put(ev, r) == null;
        if (ret) {
            if (reverseAdd) {
                ev.addPerson(this, r, false);
            }
        }
    }

    /**
     * This method adds the event with a default role (student).
     * @param reverseAdd if true the person will be added to the event as well.
     */
    public void addEvent(Event ev, boolean reverseAdd) {
        addEvent(ev, Role.STUDENT, reverseAdd);
    }

    /**
     * This method removes the event from the person.
     * @param reverseAdd if true the person will be removed from the event as well.
     */
    public boolean removeEvent(Event ev, boolean reverseRemove) {
        boolean ret = getEventsMap().remove(ev) != null;
        if (ret) {
            if (reverseRemove) {
                ev.removePerson(this, false);
            }
        }
        return ret;
    }

    @Override
    public String toString() {
        return "Person[" + super.toString() + "]";
    }
}
