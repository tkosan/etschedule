/*
 *  Copyright 2009 Peter Karich, peat_hal ‘at’ users ‘dot’ sourceforge ‘dot’ net.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */
package de.timefinder.data;

import org.joda.time.DateTime;

/**
 *
 * @author Peter Karich, peat_hal ‘at’ users ‘dot’ sourceforge ‘dot’ net
 */
public class Task {

    private String name;
    private DateTime toFinishedUntil;

    public Task(String str, DateTime toFinishedUntil) {
        name = str;
        this.toFinishedUntil = toFinishedUntil;
    }

//    public Task(Event event, IDataPoolSettings settings) {
//        name = event.getName();
//        toFinishedUntil = new DateHelper(settings).toDateTime(settings.getTimeslotsPerWeek()).
//                plus(settings.getMilliSecondsPerTimeslot() * event.getDuration());
//    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DateTime getToFinishedUntil() {
        return toFinishedUntil;
    }

    public void setToFinishedUntil(DateTime toFinishedUntil) {
        this.toFinishedUntil = toFinishedUntil;
    }

    @Override
    public String toString() {
        return getName() + (getToFinishedUntil() != null ? getToFinishedUntil() : "");
    }
}
