/*
 * This file is part of the TimeFinder project.
 * Visit http://www.timefinder.de for more information.
 * Copyright 2008 the original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.timefinder.data;

import de.timefinder.data.access.EventDao;
import de.timefinder.data.access.FeatureDao;
import de.timefinder.data.access.LocationDao;
import de.timefinder.data.access.PersonDao;
import de.timefinder.data.access.impl.EventDaoSimpl;
import de.timefinder.data.access.impl.FeatureDaoSimpl;
import de.timefinder.data.access.impl.LocationDaoSimpl;
import de.timefinder.data.access.impl.PersonDaoSimpl;
import de.timefinder.data.algo.Assignment;

/**
 * @author Peter Karich, peat_hal ‘at’ users ‘dot’ sourceforge ‘dot’ net
 */
public class TimeFinderTester {

    protected EventDao eDao;
    protected PersonDao pDao;
    protected LocationDao lDao;
    protected FeatureDao fDao;
    protected DataPool dataPool;

    public void setUp() {
        eDao = new EventDaoSimpl();
        pDao = new PersonDaoSimpl();
        lDao = new LocationDaoSimpl();
        fDao = new FeatureDaoSimpl();
        dataPool = new DataPoolImpl();
        dataPool.setDao(eDao);
        dataPool.setDao(fDao);
        dataPool.setDao(pDao);
        dataPool.setDao(lDao);
    }

    protected Event newEvent(int start, int duration) {
        Event ev = new Event();
        ev.setInterval(start, duration);
        return ev;
    }

    protected Event newEvent(String name,int start, int duration) {
        Event ev = new Event();
        ev.setInterval(start, duration);
        ev.setName(name);
        return ev;
    }

    protected Event newEvent(String name) {
        Event ev = new Event();
        ev.setName(name);
        return ev;
    }

    protected Assignment newAssignment(String name) {
        Event ev = new Event();
        ev.setName(name);
        return new Assignment(ev);
    }

    protected Assignment newAssignment(int start, int duration) {
        return newAssignment(new Event(start, duration));
    }

    protected Assignment newAssignment(Event ev) {
        return new Assignment(ev, ev.getStart());
    }

    protected Assignment newAssignment(String name, int start, int duration) {
        Event ev = new Event();
        ev.setName(name);
        ev.setDuration(duration);
        return new Assignment(ev, start);
    }

    protected Location newLocation(String str) {
        Location r = new Location();
        r.setName(str);
        return r;
    }

    protected Location newLocation(String str, int capacity) {
        Location r = newLocation(str);
        r.setCapacity(capacity);
        return r;
    }

    protected Person newPerson(String str) {
        Person p = new Person();
        p.setName(str);
        return p;
    }

    protected Feature newFeature(String str) {
        Feature f = new Feature();
        f.setName(str);
        return f;
    }
}
