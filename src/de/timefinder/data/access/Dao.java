/*
 * This file is part of the TimeFinder project.
 * Visit http://www.timefinder.de for more information.
 * Copyright 2008 the original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.timefinder.data.access;

import de.timefinder.data.informer.PropertyChangeSupport;
import java.util.Collection;
import java.util.Map;

/**
 * @author Peter Karich, peat_hal ‘at’ users ‘dot’ sourceforge ‘dot’ net
 */
public interface Dao<T> extends PropertyChangeSupport {

    public final static String DETACH = "detach";
    public final static String DETACH_ALL = "detachAll";
    public final static String ATTACH = "attach";
    public final static String ATTACH_ALL = "attachAll";

    T create();

    Class<T> getType();

    /**
     * This method attaches one object to this session. Only changes of *attached*
     * objects will be saved on commit.
     */
    T attach(T e);

    void attachAll(Collection<? extends T> collection);

    boolean detach(T t);

    void detachAll();

    /**
     * Changes to attached objects will be saved.
     */
    void commit();

    Collection<? extends T> getAll();

    Map<Long, ? extends T> getMap();

    /**
     * Searches for all objects which contains the specified string str in its name.
     */
    Collection<? extends T> findAllByName(String contain);

    /**
     * Searches for the first object which contains the specified string str in its name.
     * @return null if not found
     */
    T findFirstByName(String str);

    T findById(Long id);
}
