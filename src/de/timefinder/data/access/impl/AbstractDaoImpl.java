/*
 *  Copyright 2009 Peter Karich, peat_hal ‘at’ users ‘dot’ sourceforge ‘dot’ net.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */
package de.timefinder.data.access.impl;

import de.timefinder.data.DBInterface;
import de.timefinder.data.access.Dao;
import de.timefinder.data.informer.PropertyChangeSupportImpl;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map;
import javolution.util.FastMap;

/**
 * @author Peter Karich, peat_hal ‘at’ users ‘dot’ sourceforge ‘dot’ net
 */
public abstract class AbstractDaoImpl<T extends DBInterface>
        extends PropertyChangeSupportImpl implements Dao<T> {

    private Map<Long, T> elements;

    public AbstractDaoImpl() {
        elements = new FastMap<Long, T>();
    }

    @Override
    protected Object getSource() {
        return this;
    }

    @Override
    public Collection<T> getAll() {
        return elements.values();
    }

    @Override
    public Map<Long, T> getMap() {
        return elements;
    }

    @Override
    public boolean detach(T r) {
        if (r == null)
            return false;

        Iterator<T> iter = elements.values().iterator();
        boolean ret = false;
        while (iter.hasNext()) {
            if (iter.next() == r) {
                iter.remove();
                ret = true;
                break;
            }
        }

        if (ret)
            firePropertyChange(DETACH, r, null);

        return ret;
    }

    @Override
    public void detachAll() {
        getAll().clear();
        // what is the reason for the null value? why not old = new ArrayList(getAll())
        // 1. getAll.clear should be called before property change => new arraylist necessary
        // 2. could be very expensive only to temporarly save getAll
        // 3. ... we don't need it
        firePropertyChange(DETACH_ALL, null, getAll());
    }

    @Override
    public T attach(T t) {
        if (t == null)
            return null;

        elements.put(t.getId(), t);
        firePropertyChange(ATTACH, null, t);

        return t;
    }

    @Override
    public void firePropertyChange(String propertyName, Object old, Object newObj) {
        super.firePropertyChange(propertyName, old, newObj);
    }

    @Override
    public void attachAll(Collection<? extends T> collection) {
        setFirePropertyChange(false);
        for (T t : collection) {
            attach(t);
        }
        setFirePropertyChange(true);
        // see detachAll why we use null instead the old value
        firePropertyChange(ATTACH_ALL, null, collection);
    }

    @Override
    public Collection<T> findAllByName(String contain) {
        Collection<T> result = new ArrayList<T>();
        for (T obj : getAll()) {
            if (obj.getName() != null && obj.getName().contains(contain)) {
                result.add(obj);
            }
        }
        return result;
    }

    @Override
    public T findFirstByName(String nameToFind) {
        for (T obj : getAll()) {
            if (obj.getName() != null && obj.getName().equals(nameToFind)) {
                return obj;
            }
        }
        return null;
    }

    @Override
    public T findById(Long id) {
        return elements.get(id);
    }

    @Override
    public void commit() {
    }

    @Override
    public void refresh() {
        Map<Long, T> tmp = elements;
        elements = Collections.emptyMap();
        firePropertyChange(DETACH_ALL, tmp, Collections.EMPTY_LIST);
        elements = tmp;
        firePropertyChange(ATTACH_ALL, Collections.emptyList(), elements.values());
    }
}
