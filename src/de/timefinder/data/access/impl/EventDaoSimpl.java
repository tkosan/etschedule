/*
 * This file is part of the TimeFinder project.
 * Visit http://www.timefinder.de for more information.
 * Copyright 2008 the original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.timefinder.data.access.impl;

import de.timefinder.data.Event;
import de.timefinder.data.access.EventDao;

/**
 * @author Peter Karich, peat_hal ‘at’ users ‘dot’ sourceforge ‘dot’ net
 */
public class EventDaoSimpl extends AbstractDaoImpl<Event> implements EventDao {

    private long id = 1L;

    public EventDaoSimpl() {
    }

    @Override
    public Event attach(Event event) {
        event.setId(id++);
        return super.attach(event);
    }

    @Override
    public Class<Event> getType() {
        return Event.class;
    }

    @Override
    public Event create() {
        return new Event();
    }
}
