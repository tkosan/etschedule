/*
 * This file is part of the TimeFinder project.
 * Visit http://www.timefinder.de for more information.
 * Copyright 2008 the original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.timefinder.data.access.impl;

import de.timefinder.data.Person;
import de.timefinder.data.access.PersonDao;

/**
 * @author Peter Karich, peat_hal ‘at’ users ‘dot’ sourceforge ‘dot’ net
 */
public class PersonDaoSimpl extends AbstractDaoImpl<Person> implements PersonDao {

    private Long id = 1L;

    @Override
    public Person attach(Person person) {
        person.setId(id++);        
        return super.attach(person);
    }

    @Override
    public Class<Person> getType() {
        return Person.class;
    }

    @Override
    public Person create() {
        return new Person();
    }
}
