/*
 *  Copyright 2009 Peter Karich, peat_hal 'at' users 'dot' sourceforge 'dot' net.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */
package de.timefinder.data.algo;

import de.timefinder.data.Event;
import de.timefinder.data.Location;

/**
 * This class handles the relation ship between event, location etc
 *
 * @author Peter Karich, peat_hal 'at' users 'dot' sourceforge 'dot' net
 */
public class Assignment {

    private int start = -1;
    private Event event;
    private Location location;
    public int complexity;

    private Assignment() {
    }

    public Assignment(Event ev) {
        event = ev;
        if(ev == null)
            throw new NullPointerException("Event cannot be null!");
    }

    public Assignment(Event ev, int start) {
        this(ev);
        this.start = start;
    }

    public Assignment(Assignment ass) {
        this(ass.getEvent(), ass.getStart());
        location = ass.getLocation();
        complexity = ass.getComplexity();
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getComplexity() {
        return complexity;
    }

    public void setComplexity(int complexity) {
        this.complexity = complexity;
    }

    @Override
    public String toString() {
        return "Start:" + getStart() + " " + event + " " + location; // + " persons:" + (persons == null ? "" : persons.size());
    }
}
