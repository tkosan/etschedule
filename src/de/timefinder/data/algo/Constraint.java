/*
 *  Copyright 2009 Peter Karich, peat_hal 'at' users 'dot' sourceforge 'dot' net.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */
package de.timefinder.data.algo;

import de.timefinder.data.DBImpl;
import de.timefinder.data.Event;
import java.util.Map;

/**
 * This class saves a constraint. A weight 0 indicates that the constraint
 * should be ignored.
 *
 * @author Peter Karich, peat_hal 'at' users 'dot' sourceforge 'dot' net
 */
public class Constraint extends DBImpl {

    private float weight;

    /**
     * @return a float value which indicates the importance for the solution
     */
    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    /**
     * This method calculates the conflicts the specified solution has with this
     * constraint.
     */
    public int getViolations() {
        return 0;
    }

    @Deprecated
    public int getViolations(Object obj) {
        return 0;
    }

    public void transformEvents(Map<Event, Assignment> map) {
    }

    /**
     * This method corrects the specified solution according to the needs of this
     * constraint
     *
     * @param solution the solution which should be corrected according to the needs of
     * this constraint.
     *
     * @param time a value in [0,1] which represents the time, where 1 means so long
     * as needed and 0 means maximal an O(1) operation.
     */
    public void correct(Solution solution, float time) {
    }
}
