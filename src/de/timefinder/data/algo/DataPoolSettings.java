/*
 * This file is part of the TimeFinder project.
 * Visit http://www.timefinder.de for more information.
 * Copyright 2008 the original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.timefinder.data.algo;

import de.timefinder.data.ICalendarSettings;
import de.timefinder.data.IntervalInt;
import de.timefinder.data.IntervalLong;
import de.timefinder.data.Task;
import de.timefinder.data.set.BitRaster;
import de.timefinder.data.set.BitRasterImpl;
import de.timefinder.data.set.WeekRaster;
import de.timefinder.data.set.WeekRasterImpl;
import de.timefinder.data.util.DataPoolSettingsHelper;
import de.timefinder.data.util.RamSettings;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.joda.time.DateTime;

/**
 * @author Peter Karich, peat_hal ‘at’ users ‘dot’ sourceforge ‘dot’ net
 */
public class DataPoolSettings extends RamSettings implements ICalendarSettings {
    private final Log log = LogFactory.getLog(getClass());
    private transient DataPoolSettingsHelper helper;

    public DataPoolSettings() {
        super("Settings from one Data-Pool");
        initDefault();
    }

    public void apply(DataPoolSettings settings) {
        super.apply(settings);
        helper = null;
    }

    @Override
    protected void initDefault() {
        // set to the first monday in the current month
        setDefault("startDate", new DateTime().withDayOfMonth(1).weekOfWeekyear().roundCeilingCopy().
                plusHours(8));
        setDefault("milliSecondsPerTimeSlot", 1000 * 60 * 60L);
        setDefault("timeSlotsPerDay", 12);
        setDefault("numberOfDays", 5);
    }

    /**
     * Specifies to which start date all other relative times are calculated.
     */
    public void setStartDate(DateTime date) {
//        setObject("startDate", format.print(date));
        setObject("startDate", date);
    }

    public DateTime getStartDate() {
//        return format.parseDateTime((String) getObject("startDate"));
        return (DateTime) getObject("startDate");
    }

    /**
     * Specifies how many milliseconds will be grouped to one timeslot.
     */
    public void setMillisPerTimeslot(long val) {
        setObject("milliSecondsPerTimeSlot", val);
    }

    public long getMillisPerTimeslot() {
        return (Long) getObject("milliSecondsPerTimeSlot");
    }

    public int getTimeslotsPerWeek() {
        return getTimeslotsPerDay() * getNumberOfDays();
    }

    /**
     * Specifies how many timeslots we have for one day.
     */
    public void setTimeslotsPerDay(int val) {
        setObject("timeSlotsPerDay", val);
    }

    public int getTimeslotsPerDay() {
        return (Integer) getObject("timeSlotsPerDay");
    }

    public void setNumberOfDays(int val) {
        setObject("numberOfDays", val);
    }

    public int getNumberOfDays() {
        return (Integer) getObject("numberOfDays");
    }

    protected boolean internalContains(String key) {
        return getMap().containsKey(key);
    }

    protected String[] internalGetChildSettings() {
        return new String[]{};
    }

    protected void internalRemoveSettings() {
        throw new UnsupportedOperationException("Unsupported operation");
    }

    protected void internalSet(String key, String value) {
        getMap().put(key, value);
    }

    protected String internalGet(String key) {
        Object o = getMap().get(key);
        if (o == null) {
            return null;
        }
        return o.toString();
    }

    protected void internalRemove(String key) {
        getMap().remove(key);
    }

    public String[] getKeys() {
        return (String[]) getMap().keySet().toArray();
    }

    public void save() throws IOException {
        log.info("Save is not implemented.");
    }

    public void load() throws IOException {
        log.info("Load is not implemented.");
    }

    public boolean addListener(PropertyChangeListener l) {
        return getHelper().addListener(l);
    }

    public boolean removeListener(PropertyChangeListener l) {
        return getHelper().removeListener(l);
    }

    public void fireChanges() {
        getHelper().fireChanges();
    }

    private DataPoolSettingsHelper getHelper() {
        if (helper == null) {
            helper = new DataPoolSettingsHelper(this, null);
        }
        return helper;
    }

    @Override
    public DateTime toDateTime(int timeslot) {
        return getHelper().toDateTime(timeslot);
    }

    @Override
    public IntervalInt toInterval(DateTime start, DateTime end) {
        return getHelper().toEvent(start, end);
    }

    @Override
    public IntervalLong toIntervalLong(IntervalInt simpleInt) {
        return getHelper().toInterval(simpleInt);
    }

    @Override
    public Task toTask(IntervalInt simpleInt) {
        return getHelper().toTask(simpleInt);
    }

    public BitRaster createBooleanWeekRaster() {
        //raster = new BooleanRasterImpl(length);
        return new BitRasterImpl(getTimeslotsPerWeek());
    }

    public WeekRaster createWeekRaster() {
        return new WeekRasterImpl(createBooleanWeekRaster());
    }

    @Override
    public void setObject(String str, Object obj) {
        super.setObject(str, obj);
        fireChanges();
    }

    @Override
    public Object clone() {
        try {
            DataPoolSettings set = new DataPoolSettings();
            set.apply(this);
            set.setHelper(getHelper());
            return set;
        } catch (Exception e) {
            throw new UnsupportedOperationException("cloning failed", e);
        }
    }

    private void setHelper(DataPoolSettingsHelper helper) {
        this.helper = helper;
    }
}
