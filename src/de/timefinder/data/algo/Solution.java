/*
 * This file is part of the TimeFinder project.
 *  Visit http://www.timefinder.de for more information.
 *  Copyright (c) 2009 the original author or authors.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package de.timefinder.data.algo;

import de.timefinder.data.Event;
import de.timefinder.data.Location;
import de.timefinder.data.Person;
import java.util.Collection;
import javolution.util.FastMap;

import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;
import javolution.util.FastSet;

/**
 * This class handles one solution of the algorithm.
 *
 * @author Peter Karich, peat_hal 'at' users 'dot' sourceforge 'dot' net
 */
public class Solution {

    private Logger logger = Logger.getLogger(Solution.class.getSimpleName());
    private Map<Event, Assignment> assignments = new FastMap<Event, Assignment>();
    private int hardConflicts;
    private int softConflicts;

    //private Map<Constraint, Float> constraints = new FastMap<Constraint, Float>();
    /**
     * @return the assignment for the specified event.
     */
    public Assignment getAssignment(Event event) {
        return assignments.get(event);
    }

    /**
     * @return all assignments
     */
    public Collection<Assignment> getAssignments() {
        return assignments.values();
    }

    /**
     * All assignments will be recognized
     */
    public Collection<Person> generateAllInvolvedPersons() {
        Set<Person> ret = FastSet.newInstance();
        for (Assignment ass : assignments.values()) {
            ret.addAll(ass.getEvent().getPersons());
        }
        return ret;
    }

    /**
     * Only valid assignments will be recognized
     */
    public Collection<Location> generateAllInvolvedLocations() {
        Set<Location> ret = FastSet.newInstance();
        for (Assignment ass : assignments.values()) {
            if (ass.getStart() >= 0)
                ret.add(ass.getLocation());
        }
        return ret;
    }

    /**
     * Adds the specified assignment, but only if the associated event
     * doesn't have already a assignment!
     */
    public boolean addAssignment(Assignment assignment) {
        Assignment old = assignments.put(assignment.getEvent(), assignment);
        if (old != null) {
            assignments.put(old.getEvent(), old);
            return false;
        }
        return true;
    }

    public boolean isEventAssigned(Event event) {
        Assignment ass = assignments.get(event);
        return ass != null && ass.getStart() >= 0;
    }

    /**
     * @return number of all hard constraint violations
     */
    public int getHardConflicts() {
        return hardConflicts;
    }

    public void setHardConflicts(int hardConflicts) {
        this.hardConflicts = hardConflicts;
    }

    /**
     * @return number of all soft constraint violations
     */
    public int getSoftConflicts() {
        return softConflicts;
    }

    public void setSoftConflicts(int softConflicts) {
        this.softConflicts = softConflicts;
    }

    /**
     * @return the multidimensional distance to zero.
     */
//    public float getDistanceToZero() {
//        float res = 0;
//        float tmp;
//        for (Entry<Constraint, Float> entry : constraints.entrySet()) {
//            tmp = entry.getValue() * entry.getKey().getWeight();
//            res += tmp * tmp;
//        }
//
//        return res;
//    }
//
//    ... implements Comparable<Solution>
//    @Override
//    public int compareTo(Solution o) {
//        // Calculate the vector to zero
//        float val = getDistanceToZero();
//        float val2 = o.getDistanceToZero();
//
//        if (val < val2)
//            return -1;
//        else if (val > val2)
//            return 1;
//        else
//            return 0;
//    }

    /**
     * This method applies the stored start values to all the assigned events.
     */
    public void applyStartValuesOnEvents() {
        for (Assignment ass : assignments.values()) {
            if (ass.getLocation() != null)
                ass.getLocation().getEvents().clear();
        }

        for (Assignment ass : assignments.values()) {
            Event ev = ass.getEvent();

            if (ass.getStart() >= 0) {
                ev.setStart(ass.getStart());
                ev.setLocation(ass.getLocation());
            } else
                ev.setStart(-1);
//                ev.setLocation(null);
        }
    }

    @Override
    public String toString() {
        return "hardconstraints:" + getHardConflicts() + " softconstraints:" + getSoftConflicts();
    }
}
