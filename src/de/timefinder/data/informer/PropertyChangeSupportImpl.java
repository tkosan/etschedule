/*
 * This file is part of the TimeFinder project.
 *  Visit http://www.timefinder.de for more information.
 *  Copyright (c) 2009 the original author or authors.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package de.timefinder.data.informer;

import javolution.util.FastSet;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collection;

/**
 * @author Peter Karich, peat_hal ‘at’ users ‘dot’ sourceforge ‘dot’ net
 */
public class PropertyChangeSupportImpl implements PropertyChangeSupport {

    private Object source;
    private Collection<PropertyChangeListener> listeners = new FastSet<PropertyChangeListener>();
    private boolean firePropertyChange = true;

    @Override
    public synchronized boolean addListener(PropertyChangeListener listener) {
        if (listener == null) {
            return false;
        }
        return listeners.add(listener);
    }

    @Override
    public synchronized boolean removeListener(PropertyChangeListener listener) {
        if (listener == null) {
            return false;
        }
        return listeners.remove(listener);
    }

    public synchronized Collection<PropertyChangeListener> getListeners() {
        // prevent us from concurrent modification exception
        // if we iterate (over all listeners) and add a listener while propertyChange firing
        return new ArrayList<PropertyChangeListener>(listeners);
    }

    /**
     * This method fires the specified property change to all registered listeners
     */
    public void firePropertyChange(String propertyName, Object oldValue, Object newValue) {
        if (!getFirePropertyChange())
            return;

        if (oldValue != null && newValue != null && oldValue.equals(newValue)) {
            return;
        }
        firePropertyChange(new PropertyChangeEvent(getSource(), propertyName,
                oldValue, newValue));
    }

    /**
     * This method fires the specified event to all registered listeners
     */
    @Override
    public boolean firePropertyChange(PropertyChangeEvent evt) {
        if (!getFirePropertyChange())
            return false;

        Object oldValue = evt.getOldValue();
        Object newValue = evt.getNewValue();

        // do not return if oldValue == newValue; because both values could be
        // the same reference to a collection with different items
        if (oldValue != null && newValue != null && oldValue.equals(newValue)) {
            return false;
        }

        for (PropertyChangeListener listener : getListeners()) {
            listener.propertyChange(evt);
        }
        return true;
    }

    /**
     * @return the object to be used in PropertyEvent
     */
    protected Object getSource() {
        return source;
    }

    protected void setSource(Object source) {
        this.source = source;
    }

    @Override
    public boolean getFirePropertyChange() {
        return firePropertyChange;
    }

    /**
     * With this method the notification can be enabled/disabled
     *
     * @param firePropertyChange false if no notification should be broadcasted
     *                           to the listeners. default is true.
     */
    @Override
    public void setFirePropertyChange(boolean firePropertyChange) {
        this.firePropertyChange = firePropertyChange;
    }

    @Override
    public void refresh() {
    }
}
