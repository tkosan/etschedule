/*
 * This file is part of the TimeFinder project.
 * Visit http://www.timefinder.de for more information.
 * Copyright 2008 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.timefinder.data.meta;

import de.timefinder.data.util.MapEntry;
import java.util.ArrayList;
import java.util.Arrays;
import javolution.util.FastMap;
import java.util.List;
import java.util.Map;

/**
 * This class will be used from spring to avoid annotations and the
 * reflection stuff.
 *
 * @author Peter Karich, peat_hal ‘at’ users ‘dot’ sourceforge ‘dot’ net
 */
public class GenericObjectDescriptor {

    public Class type;
    public List<String> propertiesAsList;
    public Map<String, List<UiShow>> propertiesAsSet;

    public GenericObjectDescriptor(Class type) {
        setType(type);
    }

    public List<String> getUiProperties() {
        if (propertiesAsList == null) {
            propertiesAsList = new ArrayList<String>();
        }
        return propertiesAsList;
    }

    protected Map<String, List<UiShow>> getSet() {
        if (propertiesAsSet == null) {
            propertiesAsSet = new FastMap<String, List<UiShow>>();
        }
        return propertiesAsSet;
    }

    public void setUiProperties(MapEntry<String, List<UiShow>>... uiProperties) {
        for (MapEntry<String, List<UiShow>> prop : uiProperties) {
            addProperty(prop.getKey(), prop.getValue());
        }
    }

    public void addProperty(String key, List<UiShow> value) {
        getUiProperties().add(key);
        getSet().put(key, value);
    }

    public void addProperty(String key, UiShow... value) {
        getUiProperties().add(key);
        getSet().put(key, Arrays.asList(value));
    }

    public void setUiProperties(List<String> properties) {
        for (Object str : properties) {
            addProperty(str.toString());
        }
    }

    public Class getType() {
        return type;
    }

    public void setType(Class type) {
        this.type = type;
    }

    /**
     * This method creates a list which includes only properties which
     * do not contain the specified hide enum
     */
    public List<String> getUiProperties(UiShow hideProp) {
        List<String> props = new ArrayList<String>();
        for (String name : getUiProperties()) {
            if (!getProperty(name).contains(UiShow.ALL) &&
                    !getProperty(name).contains(hideProp)) {
                props.add(name);
            }
        }
        return props;
    }

    public List<UiShow> getProperty(String name) {
        return getSet().get(name);
    }
}
