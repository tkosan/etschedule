/*
 * BitRaster.java
 *
 * Created on 14. Oktober 2007, 18:40
 *
 * This file is part of the TimeFinder project.
 * Visit http://www.timefinder.de for more information.
 * Copyright 2008 the original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.timefinder.data.set;

/**
 * This method defines nearly the same methods which occur in BitSet.
 * But the size of this BitRaster is fixed. I.e. it will return.<br/>
 * -1 for nextFree(index) for all index >= size.<br/>
 * The return value 'false' for one index corresponds to an unassigned (free) slot
 * and 'true' correpsonds to an assigned slot.
 *
 * @author Peter Karich, peat_hal 'at' users 'dot' sourceforge 'dot' net
 */
public interface BitRaster extends Raster {
    
    /**
     * This method assignes the specified index.
     */
    void setAssignment(int index);
    
    /**
     * This method assignes the specified index if b is true, it removes
     * the assignment if b is false.
     */
    void setAssignment(int index, boolean b);
    
    /**
     * This method assignes the specified duration.
     * The specified start index is inclusive; start + duration is exclusive.
     */
    void setAssignments(int index, int duration);
    
    /**
     * This method assignes the specified duration if b is true,
     * it remvoes the assignments if b is false.
     * The specified start index is inclusive; start + duration is exclusive.
     */
    void setAssignments(int start, int duration, boolean b);
    
    /**
     * This method returns true if the spefified index is assigned ('true');
     * false otherwise.
     */
    boolean isAssigned(int index);
    
    /**
     * This method returns true is no assignments were found; false otherwise.
     */
    boolean isEmpty();
    
    /**
     * This method removes all assignments from this BitRaster.
     */
    void clear();       
    
    /**
     * This method returns the sum over all assignments in this BitRaster.
     */
    int getAssignments();
    
    /**
     * This method performs a logical 'and' - operation on this BitRaster.
     */
    void and(BitRaster raster);
    
    /**
     * This method performs a logical 'or' - operation on this BitRaster.
     */
    void or(BitRaster raster);
    
    /**
     * This method clones this instance of BitRaster.
     */
    Object clone();

    /**
     * @return a comma separated string with all assigned slots
     */
    @Override
    String toString();
}
