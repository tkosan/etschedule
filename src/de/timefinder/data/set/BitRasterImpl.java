/*
 * BitRasterImpl.java
 *
 * Created on 14. Oktober 2007, 18:50
 *
 * This file is part of the TimeFinder project.
 * Visit http://www.timefinder.de for more information.
 * Copyright 2008 the original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.timefinder.data.set;

import java.util.BitSet;

/**
 * A BitRaster implementation via BitSet.
 *
 * @see BitRaster
 * @author Peter Karich, peat_hal 'at' users 'dot' sourceforge 'dot' net
 */
public class BitRasterImpl implements BitRaster, Cloneable {

    private BitSet bitSet;
    private int iSize;

    public BitRasterImpl(int length) {
        bitSet = new BitSet(length);
        iSize = length;
    }

    public BitRasterImpl(BitSet bs, int length) {
        bitSet = bs;
        iSize = length;
    }

    public void setAssignment(int index) {
        bitSet.set(index);
    }

    public void setAssignment(int start, boolean b) {
        bitSet.set(start, b);
    }

    public void setAssignments(int start, int duration) {
        bitSet.set(start, start + duration);
    }

    public void setAssignments(int start, int duration, boolean b) {
        bitSet.set(start, start + duration, b);
    }

    public boolean isAssigned(int index) {
        return bitSet.get(index);
    }

    public boolean isEmpty() {
        return bitSet.isEmpty();
    }

    public void clear() {
        bitSet.clear();
    }

    public int getNextFree(int startSearch) {
        int ret = bitSet.nextClearBit(startSearch);
        if (ret >= iSize) {
            return iSize - 1;
        } else {
            return ret;
        }
    }

    @Override
    public int getNextFree(int startSearch, int minDuration) {
        int startGap = bitSet.nextClearBit(startSearch);
        for (; startGap >= 0 && startGap < iSize;) {
            int endGap = bitSet.nextSetBit(startGap);
            if (endGap < 0) {
                if (iSize - startGap >= minDuration) {
                    return startGap;
                }
                return -1;
            }
            if (endGap - startGap >= minDuration) {
                return startGap;
            }
            startGap = bitSet.nextClearBit(endGap);
        }

        return -1;
    }

    public int getLastFreePlus1(int startSearch) {
        if (bitSet.get(startSearch))
            return -1;

        int ret = bitSet.nextSetBit(startSearch);
        if (ret > iSize || ret < 0) {
            return iSize;
        } else {
            return ret;
        }
    }

    public int getAssignments() {
        return bitSet.cardinality();
    }

    public void and(BitRaster raster) {
        bitSet.and(((BitRasterImpl) raster).bitSet);
    }

    public void or(BitRaster raster) {
        bitSet.or(((BitRasterImpl) raster).bitSet);
    }

    @Override
    public int getLength() {
        return iSize;
    }

    @Override
    public String toString() {
        return bitSet.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof BitRasterImpl) {
            return bitSet.equals(((BitRasterImpl) obj).bitSet);
        } else if (obj instanceof BitSet) {
            throw new UnsupportedOperationException("Can't compare Raster with BitSet");
        } else if (obj instanceof BitRaster) {
            throw new UnsupportedOperationException("not yet implemented");
        } else {
            return false;
        }
    }

    @Override
    public Object clone() {
        try {
            BitRasterImpl o = (BitRasterImpl) super.clone();
            o.bitSet = (BitSet) bitSet.clone();
            return o;
        } catch (CloneNotSupportedException ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public int hashCode() {
        return bitSet.hashCode();
    }
}
