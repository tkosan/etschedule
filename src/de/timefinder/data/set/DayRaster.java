/*
 *  Copyright 2009 Peter Karich, peat_hal 'at' users 'dot' sourceforge 'dot' net.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */
package de.timefinder.data.set;

/**
 * If you add this raster to a Collection42Raster you can avoid
 * the case that one event is splitted into several days
 *
 * @author Peter Karich, peat_hal 'at' users 'dot' sourceforge 'dot' net
 */
public class DayRaster implements Raster {

    private int slotsPerDay;
    private int slotsPerWeek;

    public DayRaster(int slotsPerDay, int slotsPerWeek) {
        this.slotsPerDay = slotsPerDay;
        this.slotsPerWeek = slotsPerWeek;
    }

    @Override
    public int getNextFree(int searchFrom, int minDuration) {
        while (true) {
            if (searchFrom + minDuration > getLength()) {
                return -1;
            } else if (minDuration <= slotsPerDay - searchFrom % slotsPerDay) {
                return searchFrom;
            } else {
                searchFrom = getLastFreePlus1(searchFrom);
                continue;
            }
        }
    }

    @Override
    public int getLastFreePlus1(int searchFrom) {
        int free = (searchFrom / slotsPerDay + 1) * slotsPerDay;
        if (free <= getLength()) {
            return free;
        } else {
            return -1;
        }
    }

    @Override
    public int getLength() {
        return slotsPerWeek;
    }

    @Override
    public String toString() {
        return "length:" + slotsPerWeek + "; slots per day:" + slotsPerDay;
    }
}
