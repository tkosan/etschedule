/*
 *  Copyright 2009 Peter Karich.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */
package de.timefinder.data.set;

import de.timefinder.data.algo.Assignment;
import java.util.Set;

/**
 * This class supports the search for free slots for a certain root event.
 * All events which conflicts with the rootEvent should
 * be added to an EventRaster. Then the iterator will return all
 * gaps where the root event can be assigned without a conflict.
 * Different durations are supported.  
 * 
 * @author Peter Karich, peat_hal 'at' users 'dot' sourceforge 'dot' net
 */
public interface EventRaster extends Raster {

    void setRoot(Assignment assignment);

    /**
     * @return the root event which must be at a free slot.
     * I.e. rootEventStart cannot be assigned from added events.
     */
    Assignment getRoot();

    /**
     * Adds the specified event with specified start slot to this EventRaster.
     * @return false if the specified event could not be added. E.g. conflicts
     * with rootEvent.
     */
    boolean add(Assignment assignment);

    /**
     * Removes the specified event with specified start slot from this EventRaster.
     * @return false if the event does not exist at this slot
     */
    boolean remove(Assignment assignment);

    /**
     * This method returns all events which conflicts with root event in
     * slot range [startTime, startTime+duration)         
     */
    Set<Assignment> getConflictingAssignments(int startTime, int duration);
}
