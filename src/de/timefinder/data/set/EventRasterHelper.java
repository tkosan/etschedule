/*
 *  Copyright 2009 Peter Karich, peat_hal 'at' users 'dot' sourceforge 'dot' net.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */
package de.timefinder.data.set;

import de.timefinder.data.Event;
import de.timefinder.data.algo.Assignment;

/**
 * @author Peter Karich, peat_hal 'at' users 'dot' sourceforge 'dot' net
 */
public class EventRasterHelper {

    protected Assignment root;

    public void setRoot(Assignment ass) {
        this.root = ass;
    }

    public Assignment getRoot() {
        return root;
    }

//    public void clearRootEvent() {
//        rootEvent = null;
//    }
    public void check(Assignment ass) {
        if (root != null && Event.overlapps(ass.getEvent(), ass.getStart(), root.getEvent(), root.getStart()))
            throw new IllegalStateException("You cannot add " + ass + ", which " +
                    "stays in conflict with root event: " + root + "!");
    }

    @Override
    public String toString() {
        return "root:" + root;
    }
}
