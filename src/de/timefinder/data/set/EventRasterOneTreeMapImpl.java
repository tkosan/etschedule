/*
 * This file is part of the TimeFinder project.
 *  Visit http://www.timefinder.de for more information.
 *  Copyright (c) 2009 the original author or authors.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package de.timefinder.data.set;

import de.timefinder.data.algo.Assignment;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import javolution.util.FastSet;

/**
 * @author Peter Karich, peat_hal 'at' users 'dot' sourceforge 'dot' net
 */
public class EventRasterOneTreeMapImpl extends EventRasterHelper implements EventRaster {

    private TreeMap<Integer, FastSet<Assignment>> map = new TreeMap();
    private int length;

    public EventRasterOneTreeMapImpl(int len) {
        length = len;
    }

    @Override
    public boolean add(Assignment ass) {
        int end = ass.getEvent().getDuration() + ass.getStart();

        for (int ii = ass.getStart(); ii < end; ii++) {
            FastSet<Assignment> events = map.get(ii);
            if (events == null) {
                events = FastSet.newInstance();
                map.put(ii, events);
            }

            if (!events.add(ass)) {
                return false;
            }
        }

        return true;
    }

    @Override
    public boolean remove(Assignment ass) {
        int oldStart = ass.getStart();
        int end = ass.getEvent().getDuration() + oldStart;

        for (int ii = oldStart; ii < end; ii++) {
            FastSet<Assignment> events = map.get(ii);
            if (events == null)
                return false;

            if (!events.remove(ass))
                return false;

            if (events.size() == 0) {
                FastSet.recycle(events);
                map.remove(ii);
            }
        }

        return true;
    }

    @Override
    public Set<Assignment> getConflictingAssignments(int startTime, int duration) {
        int end = duration + startTime;
        Set<Assignment> result = FastSet.newInstance();

        for (int ii = startTime; ii < end; ii++) {
            Set<Assignment> events = map.get(ii);
            if (events != null) {
                result.addAll(events);
            }
        }

        return result;
    }

    @Override
    public int getNextFree(int startSearch, int minDuration) {
        Entry<Integer, FastSet<Assignment>> entry = map.ceilingEntry(startSearch);
        int lastIndex = startSearch;
        while (entry != null) {
            if (entry.getKey() - lastIndex >= minDuration)
                return lastIndex;

            lastIndex = entry.getKey() + 1;
            entry = map.ceilingEntry(lastIndex);
        }

        if (lastIndex >= getLength() || getLength() - lastIndex < minDuration)
            return -1;
        else
            return lastIndex;
    }

    @Override
    public int getLastFreePlus1(int startSearch) {
        if (startSearch >= getLength())
            return -1;

        Entry<Integer, FastSet<Assignment>> entry = map.ceilingEntry(startSearch);
        if (entry == null)
            return getLength();

        if (entry.getKey() >= getLength() || entry.getKey() <= startSearch)
            return -1;
        else
            return entry.getKey();
    }

    @Override
    public int getLength() {
        return length;
    }

    @Override
    public String toString() {
        return super.toString() + "; " + map.toString();
    }
}
