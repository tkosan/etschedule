/*
 * This file is part of the TimeFinder project.
 *  Visit http://www.timefinder.de for more information.
 *  Copyright (c) 2009 the original author or authors.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package de.timefinder.data.set;

import de.timefinder.data.IntervalLong;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javolution.util.FastMap;
import javolution.util.FastSet;

import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

/**
 * This class adds events and assigns them a high and an offset.
 * With that class you can create non-overlapping 2d-areas like a weekly calendar.
 *
 * @author Peter Karich, peat_hal ‘at’ users ‘dot’ sourceforge ‘dot’ net
 * @see EventStepFunction for a older version with IntervalInt
 */
public class IntervalStepFunction {

    /**
     * This map contains the association from start time to interval.
     * But e.g. if two intervals A (earlier) and B overlapp a will be added
     * to the list of intervals at B.start
     */
    private TreeMap<Long, List<IntervalLong>> startToIntervalsMap;
    // TODO do we really need this? We could use list.indexOf of the entries from startToIntervalsMap
    // or even a BiMap instead the List
    private Map<IntervalLong, Integer> intervalsToOffsetMap;

    public IntervalStepFunction() {
        startToIntervalsMap = new TreeMap<Long, List<IntervalLong>>();
        intervalsToOffsetMap = new FastMap<IntervalLong, Integer>();
    }

    public boolean addInterval(IntervalLong interval) {
        Long startSlot = interval.getStart();
        Long end = interval.getEnd();
        Collection<IntervalLong> colliding = getOverlapping(startSlot, end);

        // find-free-time-search and
        // determine maximal assignment for later usage (add on top)
        // TODO performance boost for colliding.size == 0 or 1
        int max = getMaxAssignments(interval);
        for (int offset = 0; offset < max; offset++) {
            boolean foundFreeOffset = true;
            for (IntervalLong tmp2 : colliding) {
                int offset2 = getOffset(tmp2);
                if (offset == offset2 && interval.overlapps(tmp2)) {
                    foundFreeOffset = false;
                    break;
                }
            }
            // if no other interval conflicts with that offset
            if (foundFreeOffset) {
                putInterval(interval, offset);
                // no changes to other offset or assignments are required
                return true;
            }
        }

        putInterval(interval, max);
        return true;
    }

    private void putInterval(IntervalLong interval, int offset) {
        Long end = interval.getEnd();
        Long start = interval.getStart();
        List<IntervalLong> tmpList = startToIntervalsMap.get(start);

        if (tmpList == null) {
            tmpList = new LinkedList<IntervalLong>();
            startToIntervalsMap.put(start, tmpList);
            Entry<Long, List<IntervalLong>> tmpEntry = startToIntervalsMap.lowerEntry(start);
            if (tmpEntry != null)
                for (IntervalLong itv : tmpEntry.getValue()) {
                    if (itv.getEnd() > start)
                        tmpList.add(itv);
                }
        }

        tmpList.add(interval);
        Entry<Long, List<IntervalLong>> entry = startToIntervalsMap.ceilingEntry(start);;
        while (entry != null) {
            // if interval would overlapp existing
            entry = startToIntervalsMap.higherEntry(entry.getKey());
            if (entry == null || entry.getKey() >= end)
                break;

            entry.getValue().add(interval);
        }

        intervalsToOffsetMap.put(interval, offset);
    }

    public boolean removeInterval(IntervalLong interval) {
        Long end = interval.getEnd();
        Entry<Long, List<IntervalLong>> entry = startToIntervalsMap.ceilingEntry(interval.getStart());
        if (entry == null)
            return false;

        if (intervalsToOffsetMap.remove(interval) == null)
            throw new UnsupportedOperationException("Interval should be in offset-map too!");

        List<IntervalLong> startList;

        while (entry != null && entry.getKey() < end) {
            startList = entry.getValue();
            if (!startList.remove(interval))
                throw new UnsupportedOperationException("Interval was expected in further entries of startToIntervals-map!");

            if (startList.size() == 0)
                startToIntervalsMap.remove(entry.getKey());

            entry = startToIntervalsMap.higherEntry(entry.getKey());
        }

        return true;
    }

    /**
     * This method returns the offset of the interval in this stepFunction
     * object. The offset is in the range [0, maxAssignments)
     */
    public int getOffset(IntervalLong interval) {
        Integer ret = intervalsToOffsetMap.get(interval);
        if (ret != null)
            return ret;

        throw new UnsupportedOperationException("Cannot find interval");
    }

    /**
     * This method returns the number of intervals which would overlapp with
     * the specified interval.
     */
    public int getMaxAssignments(IntervalLong interval) {
        Collection<IntervalLong> coll = getInvolved(interval.getStart(), interval.getEnd());
        int max = -1;

        for (IntervalLong tmp : coll) {
            int ret = getOffset(tmp);
            if (ret > max) {
                max = ret;
            }
        }
        return max + 1;
    }

    /**
     * This method returns all intervals in the specified range:
     * [start, start+duration)
     */
    public Collection<IntervalLong> getIntervals(Long start, Long end) {
        return getInvolved(start, end);
    }

    public boolean containsInterval(IntervalLong interval) {
        List<IntervalLong> list = startToIntervalsMap.get(interval.getStart());
        if (list != null) {
            return list.contains(interval);
        }
        return false;
    }

    public void clear() {
        startToIntervalsMap.clear();
        intervalsToOffsetMap.clear();
    }

    /**
     * This method adds all intervals to a list which are in the specified
     * range. Regardless which length the intervals have.
     */
    protected Collection<IntervalLong> getOverlapping(Long start, Long end) {
        Collection<IntervalLong> list = new FastSet<IntervalLong>();
        Entry<Long, List<IntervalLong>> entry = startToIntervalsMap.lowerEntry(start);
        Long startRange = start;
        if (entry != null)
            startRange = entry.getKey();

        for (List<IntervalLong> tmpList : startToIntervalsMap.subMap(startRange, end).values()) {
            for (IntervalLong tmpInterval : tmpList) {
                if (tmpInterval.getEnd() > start)
                    list.add(tmpInterval);
            }
        }

        return list;
    }

    /**
     * This method adds all intervals to a list which are in the specified
     * range. It will increase the range if intervals outreach this range.
     */
    public Collection<IntervalLong> getInvolved(Long start, Long end) {
        Set<IntervalLong> list = new FastSet<IntervalLong>();
        long max = end;
        Entry<Long, List<IntervalLong>> entry = startToIntervalsMap.lowerEntry(start);
        Long startRange = start;
        if (entry != null)
            startRange = entry.getKey();

        for (List<IntervalLong> eventList : startToIntervalsMap.subMap(startRange, end).values()) {
            for (IntervalLong tmpInterval : eventList) {
                if (tmpInterval.getEnd() <= start)
                    continue;

                if (max < tmpInterval.getEnd())
                    max = tmpInterval.getEnd();

                list.add(tmpInterval);
            }
        }

        if (max > end) {
            incHeightsRight(list, end, max);
        }
        return list;
    }

    /**
     * helper method for getInvolved
     */
    void incHeightsRight(Set<IntervalLong> involved_out, long start, long end) {
        long max = end;
        for (List<IntervalLong> eventList : startToIntervalsMap.subMap(start, end).values()) {
            for (IntervalLong ev : eventList) {
                if (max < ev.getEnd()) {
                    max = ev.getEnd();
                }
                involved_out.add(ev);
            }
        }
        if (max > end) {
            incHeightsRight(involved_out, end, max);
        }
    }

    @Override
    public String toString() {
        return startToIntervalsMap + " # " + intervalsToOffsetMap;
    }
}
