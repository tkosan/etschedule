/*
 *  Copyright 2009 Peter Karich, peat_hal 'at' users 'dot' sourceforge 'dot' net.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */
package de.timefinder.data.set;

import java.util.Random;

/**
 * @author Peter Karich, peat_hal 'at' users 'dot' sourceforge 'dot' net
 */
public class RandomBitRaster extends BitRasterImpl {

    private float[] probabilityArr;
    private Random random;

    public RandomBitRaster(int length) {
        super(length);
        probabilityArr = new float[length];
    }

    public void setRandom(Random random) {
        this.random = random;
    }

    public void recalc() {
        for (int ii = 0; ii < probabilityArr.length; ii++) {

            // the larger arr[ii] is the larger the possibility of the assignment
            if (random.nextFloat() < probabilityArr[ii])
                setAssignment(ii);
        }
    }

    public void reset() {
        for (int ii = 0; ii < probabilityArr.length; ii++) {
            probabilityArr[ii] = 0;
        }

        // is this faster: probabilityArr = new float[getLength()];
    }

    /**
     * Add the specified val to the probability array at index ii
     */
    public void inc(int ii, float val) {
        probabilityArr[ii] += val;

        if (probabilityArr[ii] > 1)
            probabilityArr[ii] = 1;
        else if (probabilityArr[ii] < 0)
            probabilityArr[ii] = 0;
    }
}
