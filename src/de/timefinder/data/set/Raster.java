/*
 *  Copyright 2009 Peter Karich.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */
package de.timefinder.data.set;

/**
 * @author Peter Karich, peat_hal 'at' users 'dot' sourceforge 'dot' net
 */
public interface Raster {

    /**
     * This method returns the start of the next free space.
     * E.g. If a last assignment with a duration and startTime exists, it
     * will return startTime+duration if at least "minDuration" slots are
     * available until the next assignment or until the end of this raster is reached.
     *
     * @return an integer greater or equal than the specfified startSearch<br>
     * -1 if not found
     */
    int getNextFree(int startSearch, int minDuration);

    /**
     * This method returns the last free slot and adds 1. Adding one is 
     * very convenient in for-loops.
     *
     * @return an integer greater or equal than the specified startSearch<br/>
     * -1, if no free slot is found at searchFrom<br/>
     * Returns getLength, if the raster is not assigned from startSearch until length-1
     */
    int getLastFreePlus1(int startSearch);

    /**
     * @return the total number of timeslots in this raster
     */
    int getLength();
}
