/*
 *  Copyright 2009 Peter Karich, peat_hal 'at' users 'dot' sourceforge 'dot' net.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */
package de.timefinder.data.set;

import java.util.ArrayList;
import java.util.List;

/**
 * Raster implementation which can hold several rasters.
 * It will effectivly perform an 'OR' operation to all the added rasters.
 * 
 * The performance advantage in comparison to bitSet.or(bitSet) is that
 * normally only some of the slots are checked if they are free. So not
 * a full 'OR' operation over the entire raster until its length is necessary.
 *
 * @author Peter Karich, peat_hal 'at' users 'dot' sourceforge 'dot' net
 */
public class RasterCollection implements Raster {

    private List<Raster> rasters = new ArrayList<Raster>();
    private int length;

    @Override
    public int getNextFree(int searchFrom, int minDuration) {
        return getNextFree(searchFrom, minDuration, true);
    }

    public int getNextFree(int searchFrom, int minDuration, boolean includeEventRasters) {
        Raster raster1 = null;
        if (includeEventRasters)
            raster1 = rasters.get(0);
        else {
            for (Raster raster : rasters) {
                if (raster instanceof EventRaster)
                    continue;

                raster1 = raster;
                break;
            }
        }

        if (raster1 == null)
            throw new UnsupportedOperationException("no raster were added " +
                    "or too few none eventRasters were added if includeEventRasters == false");

        int maxStart = searchFrom;
        while (true) {
            maxStart = raster1.getNextFree(maxStart, minDuration);
            if (maxStart < 0 || maxStart + minDuration > getLength())
                return -1;

            int minEnd = raster1.getLastFreePlus1(maxStart);
            if (minEnd < 0)
                return -1;

            // now go through all the other rasters and
            // make the interval from maxStart to minEnd smaller
            for (int i = 1; i < rasters.size(); i++) {
                Raster raster = rasters.get(i);
                if (!includeEventRasters && raster instanceof EventRaster)
                    continue;

                int tmpStart = raster.getNextFree(maxStart, minDuration);
                if (tmpStart < 0)
                    return -1;

                maxStart = Math.max(maxStart, tmpStart);
                if (maxStart + minDuration > getLength())
                    return -1;

                minEnd = Math.min(raster.getLastFreePlus1(maxStart), minEnd);
                if (minEnd < 0)
                    break;
            }
            if (minEnd < 0)
                return -1;
            if (minEnd - maxStart >= minDuration)
                return maxStart;

            maxStart = minEnd + 1;
        }
    }

    @Override
    public int getLastFreePlus1(int searchFrom) {
        if (rasters.size() == 0)
            throw new UnsupportedOperationException("length not initialized. no raster was added");

        Raster raster1 = rasters.get(0);
        int minLastFree = raster1.getLastFreePlus1(searchFrom);
        if (minLastFree < 0)
            return -1;

        for (int i = 1; i < rasters.size(); i++) {
            Raster raster = rasters.get(i);
            int ret = raster.getLastFreePlus1(searchFrom);
            if (ret < 0)
                return -1;

            minLastFree = Math.min(minLastFree, ret);
        }
        return minLastFree;
    }

    public void add(Raster raster) {
        if (raster.getLength() <= 0) {
            throw new IllegalArgumentException("Cannot add raster with length of " + raster.getLength());
        } else if (length == 0) {
            // the first raster assigns the value
            length = raster.getLength();
        } else if (length != raster.getLength()) {
            throw new IllegalArgumentException("Different length of rasters is not supported!");
        }
        rasters.add(raster);
    }

    public void clear() {
        rasters.clear();
    }

    @Override
    public int getLength() {
        if (rasters.size() == 0)
            throw new UnsupportedOperationException("length not initialized. no raster was added");
        return length;
    }

    @Override
    public String toString() {
        return "RasterCollection with " + rasters.size() + " rasters";
    }
}
