/*
 * WeekRaster.java
 *
 * Created on 31. Oktober 2007, 17:48
 *
 * This file is part of the TimeFinder project.
 * Visit http://www.timefinder.de for more information.
 * Copyright 2008 the original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.timefinder.data.set;

/**
 * This interface defines if an assignment for a resource or any other object
 * is required, neutral or even forbidden.
 *
 * @author Peter Karich, peat_hal 'at' users 'dot' sourceforge 'dot' net
 */
public interface WeekRaster {

    /**
     * This method sets the specified timeSlot to required, neutral, forbidden, etc.
     */
    void set(int timeSlot, RasterEnum type);

    /**
     * This method returns an BitRaster where the timeslots are true if they
     * are forbidden.
     * Then startTime of an Event shouldn't be the same as the index.     
     */
    BitRaster getForbidden();

    void setForbidden(BitRaster raster);
}
