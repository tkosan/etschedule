/*
 * WeekRasterImpl.java
 *
 * Created on 31. Oktober 2007, 18:06
 *
 * This file is part of the TimeFinder project.
 * Visit http://www.timefinder.de for more information.
 * Copyright 2008 the original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.timefinder.data.set;

/**
 * @author Peter Karich, peat_hal 'at' users 'dot' sourceforge 'dot' net
 */
public class WeekRasterImpl implements WeekRaster {

    private BitRaster raster;

    public WeekRasterImpl(BitRaster raster) {
        this.raster = raster;
    }

    private BitRaster getRaster() {
        return raster;
    }

    public void set(int timeslot, RasterEnum type) {
        getRaster().setAssignment(timeslot, type == RasterEnum.FORBIDDEN);
    }

    public BitRaster getForbidden() {
        return getRaster();
    }

    public void setForbidden(BitRaster raster) {
        this.raster = raster;
    }

    @Override
    public String toString() {
        return raster.toString();
    }
}
