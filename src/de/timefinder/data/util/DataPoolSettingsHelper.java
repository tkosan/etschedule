/*
 * This file is part of the TimeFinder project.
 *  Visit http://www.timefinder.de for more information.
 *  Copyright (c) 2009 the original author or authors.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package de.timefinder.data.util;

import de.timefinder.data.Event;
import de.timefinder.data.ICalendarSettings;
import de.timefinder.data.IntervalInt;
import de.timefinder.data.IntervalLong;
import de.timefinder.data.IntervalLongImpl;
import de.timefinder.data.Task;
import javolution.util.FastSet;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.joda.time.DateTime;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Set;

/**
 * Helper class to avoid duplicate code in IDataPoolSettings implementations,
 * because in Java multiple inheritance does not work ...
 *
 * @author Peter Karich
 */
public class DataPoolSettingsHelper {

    private Set<PropertyChangeListener> listeners;
    private ICalendarSettings settings;

    public DataPoolSettingsHelper(ICalendarSettings settings, DataPoolSettingsHelper helper) {
        this.settings = settings;
        listeners = new FastSet<PropertyChangeListener>();
        if (helper != null)
            listeners.addAll(helper.listeners);
    }

    public boolean addListener(PropertyChangeListener l) {
        return listeners.add(l);
    }

    public boolean removeListener(PropertyChangeListener l) {
        return listeners.remove(l);
    }

    public void fireChanges() {
        for (PropertyChangeListener pcl : listeners) {
            pcl.propertyChange(new PropertyChangeEvent(this, ICalendarSettings.CHANGE_ALL, null, null));
        }
    }

    public IntervalLong toInterval(IntervalInt simpleInt) {
        IntervalLong tf = new IntervalLongImpl(toDateTime(simpleInt.getStart()),
                (int) (simpleInt.getDuration()
                * settings.getMillisPerTimeslot() / ICalendarSettings.MINUTE));
        tf.setDescription(simpleInt.getDetails());
        return tf;
    }

    public DateTime toDateTime(int timeslot) {
        int startMinutes = timeslot % settings.getTimeslotsPerDay()
                * (int) (settings.getMillisPerTimeslot() / ICalendarSettings.MINUTE);

        int startDayInt = timeslot / settings.getTimeslotsPerDay();

        return settings.getStartDate().plusDays(startDayInt).plusMinutes(startMinutes);
    }
    public Log log = LogFactory.getLog(getClass());

    public IntervalInt toEvent(DateTime start, DateTime end) {
        DateTime settingsStartDate = settings.getStartDate();
        int startSlot = (int) ((start.getMillisOfDay()
                - settingsStartDate.getMillisOfDay()) / settings.getMillisPerTimeslot());
        if (startSlot < 0) {
            log.fatal("Cannot convert DateTimes - start would be negative: " + startSlot + " start=" + start + " end=" + end);
            return null;
        }
        if (startSlot >= settings.getTimeslotsPerDay()) {
            log.fatal("Cannot convert DateTimes - start would be too big: " + startSlot + " start=" + start + " end=" + end);
            return null;
        }

        int days = (int) ((start.getMillis()
                - (settingsStartDate.getMillis() - settingsStartDate.getMillisOfDay()))
                / ICalendarSettings.DAY);
        if (days < 0) {
            log.fatal("Cannot convert DateTimes - start would have negative day: " + days + " start=" + start + " end=" + end);
            return null;
        }
        if (days >= settings.getNumberOfDays()) {
            log.fatal("Cannot convert DateTimes - start would be lay outside the week; day: " + days + " start=" + start + " end=" + end);
            return null;
        }

        int duration = (int) ((end.getMillis() - start.getMillis()) / settings.getMillisPerTimeslot());
        if (duration <= 0) {
            log.fatal("Cannot convert DateTimes - duration would be non-positive: " + duration + " start=" + start + " end=" + end);
        }

        Event evOut = new Event();
        evOut.setInterval(days * settings.getTimeslotsPerDay() + startSlot, duration);
        return evOut;
    }

    public Task toTask(IntervalInt simpleInt) {
        // TODO add duration to finishedDate -> use code in toInterval
        return new Task(simpleInt.toString(), toDateTime(settings.getTimeslotsPerWeek()));
    }
}
