/*
 *  Copyright 2009 Peter Karich, peat_hal 'at' users 'dot' sourceforge 'dot' net.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */

package de.timefinder.data.util;

import java.util.Map;
import javolution.util.FastMap;

/**
 * @author Peter Karich, peat_hal 'at' users 'dot' sourceforge 'dot' net
 */
public class RamSettings {
    private Map<String, Object> defaultMap;
    private Map<String, Object> map;
    private String name;

    public RamSettings(String name) {
        this.name = name;
        defaultMap = FastMap.newInstance();
        map = FastMap.newInstance();
        initDefault();
    }

    public void setDefault(String str, Object obj) {
        defaultMap.put(str, obj);
    }

    public Object getObject(String key) {
        Object obj = map.get(key);
        if(obj == null)
            return defaultMap.get(key);
        else
            return obj;
    }    

    public void setObject(String str, Object obj) {        
        map.put(str, obj);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    protected void initDefault() {
    }

    protected Map<String, Object> getMap() {
        return map;
    }

    public void apply(RamSettings settings) {
        map = FastMap.newInstance();
        map.putAll(settings.map);

        defaultMap = FastMap.newInstance();
        defaultMap.putAll(settings.defaultMap);
        
        name = settings.name;
    }

    protected void internalRemove(String key) {
        getMap().remove(key);
    }
}
