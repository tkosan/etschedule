/*
 *  Copyright 2009 Peter Karich, peat_hal 'at' users 'dot' sourceforge 'dot' net.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */
package de.timefinder.data.util;

import de.timefinder.data.Event;
import de.timefinder.data.Resource;
import java.util.Set;
import javolution.util.FastSet;

/**
 * Normally used in testing environment, because it gives a shortcut to
 * create a resouce with events.
 * @author Peter Karich, peat_hal 'at' users 'dot' sourceforge 'dot' net
 */
public class ResourceForTest implements Resource {

    private Set<Event> events = FastSet.newInstance();

    @Override
    public Set<Event> getEvents() {
        return events;
    }

    @Override
    public String getName() {
        return "test";
    }

    @Override
    public void setName(String name) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void addEvent(Event ev, boolean reverseAdd) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean removeEvent(Event ev, boolean reverseRemove) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
