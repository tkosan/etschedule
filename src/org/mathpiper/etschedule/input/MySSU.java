/*
Copyright (c) 2016 Julius T. Kosan

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package org.mathpiper.etschedule.input;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import org.mathpiper.etschedule.model.courses.Course;
import org.mathpiper.etschedule.model.DaysTimesRoom;
import org.mathpiper.etschedule.model.courses.Section;

public class MySSU extends SemesterSchedule {

	private int lineNumber = 1;
	
	private String newCourseNumber = "";
	
	private String newCourseName = "";

	public int getLineNumber() {
		return lineNumber;
	}

	public void loadSchedule(File file) throws Exception {
		if (!file.exists()) {
			throw new IOException(file.getName() + " does not exist.");

		}

		if (!(file.isFile() && file.canRead())) {
			throw new IOException(file.getName() + " cannot be read from.");
		}

		FileInputStream fis = null;
		try {
			fis = new FileInputStream(file);
			char character = ' ';

			StringBuilder row = new StringBuilder();

			// String NL = System.getProperty("line.separator");

			Course currentCourse = null;

			String currentCourseNumber = "";

			while (fis.available() > 0) {

				int delimiterCount = 0;

				while (delimiterCount < 9) {
					character = (char) fis.read();

					if (character > 127) {
						character = ' ';
					}

					if (character == '\n') {
						lineNumber++;
					}

					row.append(character);

					if (character == '\t') {
						delimiterCount++;
					}
				}

				while (character != '\n') {
					character = (char) fis.read();

					if (character > 127) {
						character = ' ';
					}

					if (character == '\n') {
						lineNumber++;
					}

					row.append(character);
				}

				String rowString = row.toString();

				Section section = createSection(rowString);

				if (!currentCourseNumber.equals(this.newCourseNumber)) {
					currentCourseNumber = this.newCourseNumber;

					currentCourse = new Course(currentCourseNumber, this.newCourseName);

					courses.put(currentCourseNumber, currentCourse);
				}

				currentCourse.addSection(section);

				row.delete(0, row.length());

			}

		} finally {

			if (fis != null) {
				fis.close();
			}
		}
	}// end method

	private Section createSection(String row) throws Exception {
		Section section = new Section();

		String[] fields = row.split("\t");

		String compoundField = fields[0];

		String[] courseNameAndSection = compoundField.split("-");

		String courseNumber = courseNameAndSection[0].trim();

		this.newCourseNumber = courseNumber; //section.setCourseNumber(courseNumber);

		String courseSection = courseNameAndSection[1].trim();

		section.setCourseSection(courseSection);

		String courseName = fields[1].trim();

		this.newCourseName = courseName; //section.setCourseName(courseName);

		String instructors = fields[3].trim();

		section.setInstructors(instructors);

		compoundField = fields[4];

		String[] seatsOpenAndCapacity = compoundField.split("/");

		String seatsOpen = seatsOpenAndCapacity[0].trim();

		section.setSeatsOpen(Integer.parseInt(seatsOpen));

		String capacity = seatsOpenAndCapacity[1].trim();

		section.setCapacity(Integer.parseInt(capacity));

		String status = fields[5];

		System.out.println(
				courseNumber + ", " + courseSection + ", " + seatsOpen + ", " + capacity + ", " + status);

		/*
		 * if(courseNumber.equalsIgnoreCase("ETEC2101")) { int xx = 1; }
		 */

		section.setStatus(status.toLowerCase());

		String compoundDaysAndTimesAndLocations = fields[6].trim();

		String[] daysAndTimesAndLocations = compoundDaysAndTimesAndLocations.split("\n");

		/*
		 * if (courseNumber.equalsIgnoreCase("EDVA4490")) { int xx = 1;
		 * 
		 * }
		 */

		for (String daysAndTimesAndLocationComposite : daysAndTimesAndLocations) {
			// System.out.println(daysAndTimesAndLocationComposite);

			// If a day has not been assigned to a class, prepend an 'X' to the
			// string.
			if (daysAndTimesAndLocationComposite.charAt(0) >= 48 && daysAndTimesAndLocationComposite.charAt(0) <= 57) {
				daysAndTimesAndLocationComposite = "X " + daysAndTimesAndLocationComposite;
			}

			String[] daysAndTimesAndLocation = daysAndTimesAndLocationComposite.split(";");

			String daysAndTimesComposite = daysAndTimesAndLocation[0];

			String[] daysAndTimesWithoutBothMeridians = daysAndTimesComposite.split("[ -]+");

			String[] daysAndTimesWithBothMeridians = new String[5];

			daysAndTimesWithBothMeridians[0] = daysAndTimesWithoutBothMeridians[0];

			// Handle case where class spans AM and PM.
			if (daysAndTimesWithoutBothMeridians[1].endsWith("M")) {
				String timePart = daysAndTimesWithoutBothMeridians[1].substring(0,
						daysAndTimesWithoutBothMeridians[1].length() - 2);

				String meridianPart = daysAndTimesWithoutBothMeridians[1].substring(
						daysAndTimesWithoutBothMeridians[1].length() - 2, daysAndTimesWithoutBothMeridians[1].length());

				daysAndTimesWithBothMeridians[1] = timePart;

				daysAndTimesWithBothMeridians[2] = meridianPart;

				daysAndTimesWithBothMeridians[3] = daysAndTimesWithoutBothMeridians[2];

				daysAndTimesWithBothMeridians[4] = daysAndTimesWithoutBothMeridians[3];
			} else {
				daysAndTimesWithBothMeridians[1] = daysAndTimesWithoutBothMeridians[1];

				daysAndTimesWithBothMeridians[2] = new String(daysAndTimesWithoutBothMeridians[3]);

				daysAndTimesWithBothMeridians[3] = daysAndTimesWithoutBothMeridians[2];

				daysAndTimesWithBothMeridians[4] = daysAndTimesWithoutBothMeridians[3];
			}

			String days = daysAndTimesWithBothMeridians[0];
			String startTimeString = daysAndTimesWithBothMeridians[1];
			String startTimeMeridian = daysAndTimesWithBothMeridians[2];
			String endTimeString = daysAndTimesWithBothMeridians[3];
			String endTimeMeridian = daysAndTimesWithBothMeridians[4];

			DaysTimesRoom daysTimesRoom = new DaysTimesRoom(days, startTimeString, startTimeMeridian, endTimeString,
					endTimeMeridian, "MAIN CAMPUS", "BBB", "RRR");

			// System.out.println(daysAndTimes);

			section.addDaysTimesRoom(daysTimesRoom);
			
			LocalDate startDate = LocalDate.parse(fields[8], dateFormatter);
			section.setStartDate(startDate);
			
			LocalDate endDate = LocalDate.parse(fields[9], dateFormatter);
			section.setEndDate(endDate);

		} // end for.

		String creditHours = fields[7].trim();
		section.setCreditHours(new BigDecimal(creditHours));

		// System.out.println(section.toString());

		// System.out.println("---------");

		return section;

	}// end method.

	

	// (def zz {"BUAC1010"
	// [{"01",[[80,168,20],],},{"02",[[80,120,20],],},{"51",[[16,222,40],],},]
	// })

	

	// :ARTS4222 {:name "Advanced Life Drawing 2" :sections {:01
	// {:days-and-times [[80 186 15] ] } :faculty ["Reynolds, Todd" ] }}

	public static void main(String[] args) {

		// SemesterScheduleCindy schedule = new SemesterScheduleCindy(); //Excel
		// spreadsheet.
		MySSU schedule = new MySSU(); // Copy/Paste from
															// myssu.

		// File scheduleDSV = new

		File scheduleDSV = new File("/home/tkosan/ssu2/cindy_schedule/tsv/fall_2016_created_on_july_29th_v2.csv");
		//File scheduleDSV = new File("/home/tkosan/ssu2/cindy_schedule/tsv/fall_2016_created_on_june_22nd.tsv");

		
		// File scheduleDSV = new
		// File("/home/tkosan/ssu2/cindy_schedule/tsv/Fall 2014 Master Course
		// Listing (1st Draft).xlsx");
		try {
			schedule.loadSchedule(scheduleDSV);

			String output = schedule.toClojureMaps(scheduleDSV.getName());
			//String output = schedule.toMathPiper(scheduleDSV.getName());
			//String output = schedule.toXML(scheduleDSV.getName());


			BufferedWriter writer = null;
			try {
				writer = new BufferedWriter(
						new FileWriter("/home/tkosan/ssu2/cindy_schedule/tsv/ssu_semester_schedule_map.clj"));
				
						//new FileWriter("/home/tkosan/ssu2/cindy_schedule/tsv/fall_2016_created_on_june_11th.mpwnl"));
						//new FileWriter("/home/tkosan/ssu2/cindy_schedule/tsv/fall_2016_created_on_june_22nd.xml"));
				writer.write(output);

			} catch (IOException e) {
			} finally {
				try {
					if (writer != null)
						writer.close();
				} catch (IOException e) {
				}
			}

		} catch (Exception e) {

			System.out.println(schedule.getLineNumber());
			e.printStackTrace();
		}
	}

}
