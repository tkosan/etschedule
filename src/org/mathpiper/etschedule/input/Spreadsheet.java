/*
Copyright (c) 2016 Julius T. Kosan

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

package org.mathpiper.etschedule.input;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.mathpiper.etschedule.model.courses.Course;
import org.mathpiper.etschedule.model.DaysTimesRoom;
import org.mathpiper.etschedule.model.courses.Section;

public class Spreadsheet extends SemesterSchedule {

	private int lineNumber = 0;

	private final int NEW_SECTION = 0;
	private final int COURSE_NUMBER = 2;
	private final int SECTION_NUMBER = 3;
	private final int COURSE_NAME = 4;
	private final int CREDIT_HOURS = 5;
	private final int INSTRUCTOR = 6;
	private final int DAYS = 7;
	private final int TIMES = 8;
	private final int BUILDING = 9;
	private final int ROOM_NUMBER = 10;
	
	private String newCourseNumber = "";
	
	private String newCourseName = "";

	private String strLine;

	public int getLineNumber() {
		return lineNumber;
	}

	public void loadSchedule(File file) throws Exception {
		if (!file.exists()) {
			throw new IOException(file.getName() + " does not exist.");

		}

		if (!(file.isFile() && file.canRead())) {
			throw new IOException(file.getName() + " cannot be read from.");
		}

		FileInputStream fis = null;
		try {
			fis = new FileInputStream(file);
			char character = ' ';

			Course currentCourse = null;

			String currentCourseNumber = "";

			DataInputStream in = new DataInputStream(fis);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));

			Section section = null;

			while ((strLine = br.readLine()) != null) {
				lineNumber++;

				//System.out.println(strLine);

				if (strLine.contains("*")) {
					continue;
				}

				if (strLine.contains("TBA") && !strLine.contains("ONLI")) {
					continue;
				}

				String[] fields = strLine.split("\t");

				if (fields.length != 0 && fields.length >= DAYS + 1
						&& (fields[DAYS].contains("-") || fields[DAYS].contains("M") || fields[DAYS].contains("T")
								|| fields[DAYS].contains("W") || fields[DAYS].contains("R")
								|| fields[DAYS].contains("F"))) {

					this.newCourseNumber = fields[COURSE_NUMBER];


					String sectionNumber = fields[SECTION_NUMBER];
					this.newCourseName = fields[COURSE_NAME];
					BigDecimal creditHours = new BigDecimal(-1);
					try {
						creditHours = new BigDecimal(fields[CREDIT_HOURS]);
					} catch (NumberFormatException nfe) {
						//int xx = 1; System.out.println(xx);
					}
					String instructor = fields[INSTRUCTOR];
					String days = fields[DAYS];
					String times = fields[TIMES];
					String building = fields[BUILDING];
					String roomNumber = fields[ROOM_NUMBER];

					if (fields[NEW_SECTION].equals("X")) {
						continue;
					}

					if (fields[NEW_SECTION].equals("O")) {

						section = new Section();

						//section.setCourseNumber(courseNumber);
						section.setCourseSection(sectionNumber);
						//section.setCourseName(courseName);
						section.setInstructors(instructor.toLowerCase());
						section.setCreditHours(creditHours);
						section.setStatus("open");

						addDaysTimesRoom(days, times, section, building, roomNumber);

					} else {
						addDaysTimesRoom(days, times, section, building, roomNumber);
					}

					if (section != null && fields[NEW_SECTION].equals("O")) {

						if (!currentCourseNumber.equals(this.newCourseNumber)) {
							currentCourseNumber = this.newCourseNumber;
		
							currentCourse = new Course(currentCourseNumber, this.newCourseName);
		
							courses.put(currentCourseNumber, currentCourse);
						}

						currentCourse.addSection(section);


					} // end if.

				} // end if (fields.length != 0)

			} // end while.

		} catch (Exception e) {
			throw e;
		} finally {

			if (fis != null) {
				fis.close();
			}
		}
	}// end method

	private void addDaysTimesRoom(String days, String times, Section section, String building, String roomNumber) {

		String startTimeString;
		String startTimeMeridian;
		String endTimeString;
		String endTimeMeridian;

		if (times.endsWith("M")) {

			String[] timesWithoutBothMeridians = times.split("[-]+");

			endTimeString = timesWithoutBothMeridians[1].substring(0, timesWithoutBothMeridians[1].length() - 2).trim();

			endTimeMeridian = timesWithoutBothMeridians[1]
					.substring(timesWithoutBothMeridians[1].length() - 2, timesWithoutBothMeridians[1].length()).trim();

			startTimeString = timesWithoutBothMeridians[0];

			if (endTimeMeridian.equalsIgnoreCase("AM")) {
				startTimeMeridian = "AM";
			} else {
				String[] startTimeHourMinuteString = startTimeString.split(":");
				String startTimeHourString = startTimeHourMinuteString[0];
				int startTimeHour = Integer.parseInt(startTimeHourString);

				String[] endTimeHourMinuteString = endTimeString.split(":");
				String endTimeHourString = endTimeHourMinuteString[0];
				int endTimeHour = Integer.parseInt(endTimeHourString);

				if (startTimeHour >= 8 && startTimeHour < 12 && (endTimeHour < 5 || endTimeHour == 12)) {
					startTimeMeridian = "AM";
				} else {
					startTimeMeridian = "PM";
				}
			}
		} else {
			startTimeString = "0:0";
			startTimeMeridian = "XX";
			endTimeString = "0:0";
			endTimeMeridian = "XX";

		}
		try {
			DaysTimesRoom daysTimesRoom = new DaysTimesRoom(days.replaceAll("-", ""), startTimeString,
					startTimeMeridian, endTimeString, endTimeMeridian, "", building, roomNumber);

			if (section != null) {
				section.addDaysTimesRoom(daysTimesRoom);
			} else {
				System.out.println("Null Section object. Line: " + lineNumber);
			}
		} catch (Exception e) {
			System.out.println(
					"Parsing exception: " + e.getMessage() + "  On line: " + lineNumber + "  Line: " + strLine);

		}
	}// end method.



	public static void main(String[] args) {

	File scheduleDSV = new File("/home/tkosan/ssu2/cindy_schedule/tsv/ET Only Spring 2017 Schedule 2nd Draft.tsv");
        //File scheduleDSV = new File("/home/tkosan/ssu2/fall_2020/myssu_schedule_snapshot_8_18_2020.tsv"); //todo:tk:the myssu converter needs to be updated for this file.
        
		// File scheduleDSV = new File("Fall 2013 Master Course Listing 2nd
		// Run.tsv");
		
		try {
			Spreadsheet schedule = new Spreadsheet();

			schedule.loadSchedule(scheduleDSV);
			
			schedule.instructorScheduleReport(schedule, scheduleDSV.getName());
                        
                        schedule.roomScheduleShow("326", 100, 100);
			
			//schedule.instructorWorkloadReport(schedule, scheduleDSV.getName());
		} catch (Exception e) {
			// TODO Auto-generated catch block

			//System.out.println("Error on line " + schedule.getLineNumber());
			e.printStackTrace();
		}
	/*
		try {
			
			XML xmlReader = new XML();

			//String filename = "/home/tkosan/ssu2/uis/courses_8_3_2016_3_51_am.xml";
			//String filename = "/home/tkosan/ssu2/uis/courses_8_21_2016_3_50_pm.xml";
			//String filename = "/home/tkosan/ssu2/uis/courses_8_23_2016_5_4_pm.xml";
			String filename = "/home/tkosan/ssu2/uis/courses_8_26_2016_3_44_pm.xml";
			
			try {
				xmlReader.parse(filename);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			System.out.print("\n\n");
			
			//System.out.println(scheduleDSV.getName() + "\n");
			System.out.println(filename + "\n");
			
			int totalEnrolled = 0;
			
			// schedule.instructorScheduleReport(schedule, scheduleDSV.getName());
			
			//String[] courseNames = {"MATH0101", "MATH0102", "MATH0120A", "MATH1200A", "MATH1200"}; //"MATH0102A", 
			
			//String[] courseNames = {"ETCO1120"};
			
			//String[] courseNames = {"BUIS1010"};
			
			String[] courseNames = {"UNIV1100"};
			
			//String[] courseNames = {"MATH0101"};
			
			
			for(String courseName : courseNames)
			//Set<String> allCourses = xmlReader.courses.keySet();for(String courseName : allCourses)
			{
				System.out.print(courseName + ": "); 
				Course course = xmlReader.courses.get(courseName);
				
				List<Section> sections = course.getSections();
				
				int totalSectionEnrolled = 0;
				
				for (Section section : sections) {
					
					int capacity = section.getCapacity();
					
					int seatsOpen = section.getSeatsOpen();

					totalSectionEnrolled += capacity - seatsOpen;
				}
				
				System.out.println(totalSectionEnrolled);
				
				totalEnrolled += totalSectionEnrolled;
			
			}
			
			System.out.println();
			System.out.println("Total Section Enrollments: " + totalEnrolled);

		} catch (Exception e) {
			// TODO Auto-generated catch block

			//System.out.println("Error on line " + schedule.getLineNumber());
			e.printStackTrace();
		}
		*/
	}

}
