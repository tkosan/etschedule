package org.mathpiper.etschedule.input;

import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.math.BigDecimal;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Map;
import java.util.Stack;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.mathpiper.etschedule.model.courses.Course;
import org.mathpiper.etschedule.model.DaysTimesRoom;
import org.mathpiper.etschedule.model.courses.Instructor;
import org.mathpiper.etschedule.model.courses.Section;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class XML extends SemesterSchedule {

    private XMLHandler handler = new XMLHandler();
    
    
    public XML()
    {
        super();
    }
    
    public XML(Map<String, Course> courses)
    {
        this.courses = courses;
    }

    public void parse(String filename) throws Exception {
        handler.parse(filename);
    }

    public static void main(String[] argv) {
        
        XML xmlReader = new XML();

        //String filename = "/home/tkosan/ssu2/uis/courses_9_2_2017_10_05_pm.xml";
        //String filename = "/home/tkosan/ssu2/uis/courses_8_23_2016_5_4_pm.xml";
        //String filename = "/home/tkosan/ssu2/uis/courses_8_17_2018_10_37_am.xml";
        //String filename = "/home/tkosan/ssu2/uis/courses_1_12_2018_3_27_pm.xml";
         
        //String filename = "/home/tkosan/ssu2/uis/courses_12_20_2018_4_07_pm.xml";
        //String filename = "/home/tkosan/ssu2/spring_2019/optiplanner_test_spring_2019.xml";
        
        //String filename = "/home/tkosan/ssu2/uis/9_10_2019_2_36_pm.xml";
        
        //String filename = "/home/tkosan/ssu2/uis/courses_10_17_2019_1_52_am.xml";
        
        //String filename = "/home/tkosan/ssu2/uis/courses_8_17_2018_10_37_am.xml";
        
                
        //String filename = "/home/tkosan/ssu2/uis/courses_9_2_2017_10_05_pm.xml";

        try {
            //xmlReader.parse(filename);
            
            /*
            String filename = "/home/tkosan/ssu2/uis/courses_8_17_2018_10_37_am.xml";
            xmlReader.parse(filename);
            System.out.println(filename);
            xmlReader.totalEnrollmentReport();
            xmlReader.sectionsReport();
            
            System.out.println("\n\n\n==================================================\n\n\n");
            
            filename = "/home/tkosan/ssu2/uis/9_10_2019_2_36_pm.xml";
            xmlReader.parse(filename);
            System.out.println(filename);
            xmlReader.totalEnrollmentReport();
            xmlReader.sectionsReport();
            */
            //xmlReader.duplicateSectionReport();
            
            
            
            /*
            xmlReader.instructorScheduleShow(new String[] {
                "tkosan2@shawnee.edu"
            }, 
                
                new String[] 
                {   
                //"M 2:00 - 3:00 PM", 
                //"M 4:45 - 5:30 PM",
                //"T 4:10 - 5:30 PM",
                ////"W 2:00 - 2:30 PM",
                //"W 5:05 - 5:30 PM",
                
                //"R 4:10 - 5:10 PM",
                //Duane
                //"M 9:00 - 11:08 AM",
                //"W 9:00 - 11:08 AM",
                //"M 12:00 - 2:08 PM",
                //"W 12:00 - 2:08 PM",
                //"M 5:30 - 7:38 PM",
                //"W 5:30 - 7:38 PM",
                //"M 5:30 - 7:38 PM",
                //"T 12:00 - 2:08 PM",
                //"R 12:00 - 2:08 PM",
                }, 
                true, 100, 60); //150, 60
         //*/
         
         
        /*
         xmlReader.instructorScheduleShow(new String[] {"tkosan2@shawnee.edu"}, new String[]
            {   
            "M 5:30 - 9:55 PM, ETEC1101, 51, ATC326",
            "T 2:00 - 4:15 PM, ETEC2311, 01, ATC326",
            "R 2:00 - 4:15 PM, ETEC2311, 01, ATC326",
            "T 5:30 - 9:55 PM, ETEC1302, 51, ATC326",
            "W 5:30 - 9:55 PM, ETEC4502, 51, ATC326",
            "R 5:30 - 9:55 PM, ETCO1120, 51, ATC327",
            "M 4:30 - 5:30 PM,,,Office",
            "T 4:30 - 5:30 PM,,, Office",
            "W 4:30 - 5:30 PM,,, Office",
            "R 4:30 - 5:30 PM,,, Office",

           }, true, 150, 90);
        */
        
        
         xmlReader.instructorScheduleShow(new String[] {"tkosan2@shawnee.edu"}, new String[]
            {   
            "M 5:30 - 9:55 PM, ETEC2601, 51, ATC327",
            "T 5:30 - 9:55 PM, EEC3501, 51, ATC327",
            "W 5:30 - 9:55 PM, ETEC2301, 51, ATC326",
            "M 4:10 - 5:30 PM,,,Office",
            "T 4:10 - 5:30 PM,,, Office",
            "W 4:10 - 5:30 PM,,, Office",

           }, true, 150, 90);
         
         
        
        /*
        
         

            //xmlReader.roomScheduleShow("ATC110");
            
            //System.out.println(xmlReader.roomSchedule("ATC110"));
            
            
            //Map<String,Course> coursesSubset = xmlReader.coursesSubset(new String[] {"ETEC4501","ETEC2301","ETEC2601","ETEM1110","ETCO1310","ETEC2110","ETEC3401","ETEM2516","ETEC3701","ETEM2514","ETEC3501","ETEC3402","ETCO1120","ETCO2210"});
            //Map<String,Course> coursesSubset = xmlReader.coursesSubset(new String[] {"ETCO1120","ETEC1302","ETEC1101","ETEM1216","ETEM1111"});
            
            //Map<String, Course> coursesSubset = xmlReader.coursesSubset("ET");
            //XML etXML = new XML(coursesSubset);
            //System.out.println(etXML.toXML(filename));
            
            //etXML.allCourseNumbersReport();
            //*/
            
            
            
            //xmlReader.totalSectionMinutesReport();

            //--------------------------------------------------
            //xmlReader.instructorScheduleReport(xmlReader, "foo");
            //xmlReader.differentCreditHoursReport();
            //xmlReader.instructorWorkloadReport(xmlReader, "foo");
            //System.out.println(xmlReader.roomSchedule("ATC110"));
            /* Quantitative reasoning category enrollment by class report.
			String[] quantativeReasoningCourses = {"MATH1100","MATH1170","MATH1200","MATH1200A","MATH1250","MATH1900","MATH2110","STAT1150","STAT1800","STAT2500"}; 
			Map<String, Integer> map = new HashMap();
			for(String course:quantativeReasoningCourses)
			{
				map.put(course, xmlReader.getTotalClassEnrollment(course));
			}
			Map sortedMap = xmlReader.sortByValue(map);
			Collection<String> names = sortedMap.keySet();
			for(String name:names)
			{
				System.out.println(name + ", " + sortedMap.get(name));
			}
             */
 /*		
			// Faculty open meeting times
			
			DayAndTimeOverlap overlap = new DayAndTimeOverlap();
			Instructor instructor = null;
			List<Section> sections = null;
			
			String[] instructors = {"tkosan2", "jhudson", "bteeters"};
			
			for(String name:instructors)
			{
				
				instructor = new Instructor();
				instructor.setFirstName(name);
				instructor.setMiddleInitial("");
				instructor.setLastName("");
				
				instructor.setEmailAddress(name + "@shawnee.edu");
				String schedule = xmlReader.instructorSchedule(instructor, false);
				System.out.println(schedule);
				
				instructor = xmlReader.instructorGet(xmlReader, name);
				sections = xmlReader.instructorHoursSections(instructor);
				overlap.loadTimeBlocks(sections);
			}
			
			
			overlap.findOpenTimeBlocks("M", 5);
			
			System.out.println();
			
			overlap.findOpenTimeBlocks("T", 5);
			
			System.out.println();
			
			overlap.findOpenTimeBlocks("W", 5);
			
			System.out.println();
			
			overlap.findOpenTimeBlocks("R", 5);
			
			System.out.println();
		
			//overlap.findOpenTimeBlocks("F", 5);
		
//*
			
			
			
		
			/* Student's compatible schedules */
 /*		 
			DayAndTimeOverlap overlap = null;

			List<Section> sections = null;
			
			StringBuilder facultySchedules = new StringBuilder();

			FileInputStream fis = new FileInputStream("/home/tkosan/a_ssu/Kyle research/cet_student_schedules/cet_student_schedules.csv");
			 
			//Construct BufferedReader from InputStreamReader
			BufferedReader br = new BufferedReader(new InputStreamReader(fis));
			
			List<Section> samsSections = new ArrayList();
			boolean isSamsSections = true;
			int facultySchedulesCounter = 0;
		 
			String line = null;
			while ((line = br.readLine()) != null) {
				System.out.println(line);
				
				if(line.charAt(0) == '0')
				{
					System.out.println("Sam's Schedule");
					overlap = new DayAndTimeOverlap();
				}
				else if(line.charAt(0) >= '0' && line.charAt(0) <= '9')
				{
					overlap = new DayAndTimeOverlap();
					
					
					//Load Sam's sections.
					for(Section section:samsSections)
					{
						overlap.loadTimeBlocks(section);
					}
					
					
					String[] instructors = {"kvick", "tkosan2"};
					
					Instructor instructor = null;
					
					for(String name:instructors)
					{
						instructor = new Instructor();
						instructor.setFirstName(name);
						instructor.setMiddleInitial("");
						instructor.setLastName("");
						
						instructor.setEmailAddress(name + "@shawnee.edu");
						String schedule = xmlReader.instructorSchedule(instructor);
						
						instructor = xmlReader.instructorGet(xmlReader, name);
						List<Section> facultySections = xmlReader.instructorHoursSections(instructor);
						overlap.loadTimeBlocks(facultySections);
					}
				}

				
				if(line.charAt(0) == 'x')
				{
					isSamsSections = false;
					String[] instructors = {"kvick", "tkosan2"};
					
					Instructor instructor = null;
					
					for(String name:instructors)
					{
						instructor = new Instructor();
						instructor.setFirstName(name);
						instructor.setMiddleInitial("");
						instructor.setLastName("");
						
						instructor.setEmailAddress(name + "@shawnee.edu");
						String schedule = xmlReader.instructorSchedule(instructor, false);
						
						System.out.println(schedule);
						
						instructor = xmlReader.instructorGet(xmlReader, name);
						List<Section> facultySections = xmlReader.instructorHoursSections(instructor);
						overlap.loadTimeBlocks(facultySections);
					}
					System.out.println("\n\n===============================================================");
				}

				
				if(line.charAt(0) == '\t')
				{
					
					overlap.findOpenTimeBlocks("M", 5);
					
					System.out.println();
					
					overlap.findOpenTimeBlocks("T", 5);
					
					System.out.println();
					
					//overlap.findOpenTimeBlocks("W", 5);
					
					//System.out.println();
					
					overlap.findOpenTimeBlocks("R", 5);
					
					System.out.println();
				
					//overlap.findOpenTimeBlocks("F", 5);
				
			
					System.out.println("\n\n===============================================================");
					
				}
				else if(line.charAt(0) >= 'A' && line.charAt(0) <= 'Z')
				{
					String courseNumber = line.substring(0, 8);
					Course course = xmlReader.courses.get(courseNumber);
					String sectionNumber = line.substring(9, 11);
					sections = course.getSections();
					
					for(Section section:sections)
					{
						if(section.getCourseSection().equals(sectionNumber))
						{
							if(isSamsSections)
							{
								samsSections.add(section);
							}
							else
							{
								overlap.loadTimeBlocks(section);
							}
						}
						

					}
					
				}

			}
		 
			br.close();

		    overlap.loadTimeBlocks(sections);
			
			
			overlap.findOpenTimeBlocks("M", 5);
			
			System.out.println();
			
			overlap.findOpenTimeBlocks("T", 5);
			
			System.out.println();
			
			overlap.findOpenTimeBlocks("W", 5);
			
			System.out.println();
			
			overlap.findOpenTimeBlocks("R", 5);
			
			System.out.println();
		
			//overlap.findOpenTimeBlocks("F", 5);
		
	//*/
            // ***************
            //	xmlReader.capacityReport();
            //System.out.println(xmlReader.getTotalClassEnrollment("ETGG1801"));
            //xmlReader.totalEnrollmentReport();
            //xmlReader.sectionsReport();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public void createClojureMap(String filename, String dateString) throws Exception {
        String output = toClojureMaps(dateString);

        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(
                    new FileWriter(filename));

            //new FileWriter("/home/tkosan/ssu2/cindy_schedule/tsv/fall_2016_created_on_june_11th.mpwnl"));
            //new FileWriter("/home/tkosan/ssu2/cindy_schedule/tsv/fall_2016_created_on_june_22nd.xml"));
            writer.write(output);

        } catch (IOException e) {
        } finally {
            try {
                if (writer != null) {
                    writer.close();
                }
            } catch (IOException e) {
                throw e;
            }
        }
    }

    @Override
    public void loadSchedule(File file) throws Exception {
        // TODO Auto-generated method stub

    }

    @Override
    public int getLineNumber() {
        // TODO Auto-generated method stub
        return 0;
    }

    public class XMLHandler extends DefaultHandler {

        private String startTimeTemp;

        private Stack elementStack = new Stack();

        private Stack objectStack = new Stack();

        private Locator locator;

        private SimpleDateFormat dateParser = new SimpleDateFormat("MM/dd/yyyy");

        private Writer out;

        private StringBuffer textBuffer = new StringBuffer();

        public void setDocumentLocator(Locator locator) {
            this.locator = locator;
        }

        // SAX DocumentHandler methods
        public void startDocument() throws SAXException {
        }

        public void endDocument() throws SAXException {
        }

        public void startElement(String namespaceURI, String sName, // simple name
                String qName, // qualified name
                Attributes attributes) throws SAXException {

            //Push it in element stack
            this.elementStack.push(qName);

            if ("course".equalsIgnoreCase(qName)) {
                Course course = new Course();

                this.objectStack.push(course);
            } else if ("section".equalsIgnoreCase(qName)) {

                Section section = new Section();

                if (attributes != null) {
                    int seatsOpen = Integer.parseInt(attributes.getValue("seats_open"));
                    section.setSeatsOpen(seatsOpen);

                    int capacity = Integer.parseInt(attributes.getValue("capacity"));
                    section.setCapacity(capacity);

                    String status = attributes.getValue("status");
                    section.setStatus(status);

                    String honorsStatus = attributes.getValue("honors");
                    section.setIsHonors("true".equalsIgnoreCase(honorsStatus));
                }

                this.objectStack.push(section);
            } else if ("location".equalsIgnoreCase(qName)) {
                DaysTimesRoom daysTimesRoom = new DaysTimesRoom();

                this.objectStack.push(daysTimesRoom);
            } else if ("instructor".equalsIgnoreCase(qName)) {
                Instructor instructorOfRecord = new Instructor();

                this.objectStack.push(instructorOfRecord);
            }

        }

        public void endElement(String namespaceURI, String sName, // simple name
                String qName // qualified name
        ) throws SAXException {

            String value = textBuffer.toString();

            if (this.objectStack.size() != 0) {
                processEndTags(value, qName);
            }

            this.elementStack.pop();
            textBuffer.delete(0, textBuffer.length());

        }

        private void processEndTags(String value, String qName) throws SAXException {
            
            value = value.trim();
            
            Object object = this.objectStack.peek();

            if (object instanceof Course) {
                if ("number".equals(currentElement())) {
                    Course course = (Course) object;
                    course.setNumber(value);
                } else if ("name".equals(currentElement())) {
                    Course course = (Course) object;
                    course.setName(value);
                }
            } else if (object instanceof Section) {
                Section section = (Section) object;

                if ("number".equals(currentElement())) {
                    section.setCourseSection(value);
                } else if ("credit_hours".equals(currentElement())) {
                    section.setCreditHours(new BigDecimal(value));
                } else if ("start_date".equals(currentElement())) {
                    try {
                        LocalDate startDate = LocalDate.parse(value, dateFormatter);
                        section.setStartDate(startDate);
                    } catch (Exception e) {
                        SAXException saxException = new SAXException();
                        saxException.addSuppressed(e);
                        throw saxException;
                    }
                } else if ("end_date".equals(currentElement())) {
                    try {
                        LocalDate endDate = LocalDate.parse(value, dateFormatter);
                        section.setEndDate(endDate);
                    } catch (Exception e) {
                        SAXException saxException = new SAXException();
                        saxException.addSuppressed(e);
                        throw saxException;
                    }
                } else if ("section".equals(currentElement())) {
                    this.objectStack.pop(); // Pop the section object.

                    Course course = (Course) this.objectStack.peek();

                    if (!isBlacklistSection(course, section)) {
                        course.addSection(section);
                    }
                }
            } else if (object instanceof DaysTimesRoom) {
                DaysTimesRoom daysTimesRoom = (DaysTimesRoom) object;

                if ("days".equals(currentElement())) {
                    try {
                        daysTimesRoom.setDays(value);
                    } catch (Exception e) {
                        SAXException saxException = new SAXException();
                        saxException.addSuppressed(e);
                        throw saxException;
                    }
                } else if ("start_time".equals(currentElement())) {
                    this.startTimeTemp = value;
                } else if ("end_time".equals(currentElement())) {
                    try {
                        String startTime = this.startTimeTemp;

                        String endTime = value;

                        daysTimesRoom.setStartTimeAndDurationMilitary(startTime, endTime);
                    } catch (Exception e) {
                        SAXException saxException = new SAXException("Line number: " + locator.getLineNumber() + ", Column Number: " + locator.getColumnNumber());
                        saxException.addSuppressed(e);

                        throw saxException;
                    }
                } else if ("site".equals(currentElement())) {
                    daysTimesRoom.setSite(value);
                } else if ("building".equals(currentElement())) {
                    daysTimesRoom.setBuilding(value);
                } else if ("room_number".equals(currentElement())) {
                    daysTimesRoom.setRoomNumber(value);
                } else if ("location".equals(currentElement())) {
                    this.objectStack.pop(); // Pop the DaysTimesRoom object.

                    Section section = (Section) this.objectStack.peek();

                    section.addDaysTimesRoom((DaysTimesRoom) object);

                }
            } else if (object instanceof Instructor) {
                Instructor instructor = (Instructor) object;

                if ("last_name".equals(currentElement())) {
                    instructor.setLastName(value);
                } else if ("first_name".equals(currentElement())) {
                    instructor.setFirstName(value);
                } else if ("middle_initial".equals(currentElement())) {
                    instructor.setMiddleInitial(value);
                } else if ("school_email_address".equals(currentElement())) {
                    instructor.setEmailAddress(value);
                } else if ("instructor".equals(currentElement())) {
                    this.objectStack.pop(); // Pop the Instructor object.

                    Section section = (Section) this.objectStack.peek();

                    section.setInstructorOfRecord((Instructor) object);
                }
            }

            if ("course".equals(qName)) {
                Course course = (Course) this.objectStack.pop();

                courses.put(course.getNumber(), course);
            }

        }

        //todo:tk:tempory blacklist method.
        private String[] blacklist = {}; //{"PSYC1101-01", "PTAT2190-01", "EDUC5502-01"};

        private boolean isBlacklistSection(Course course, Section section) {
            boolean isBlacklist = false;

            for (String entry : blacklist) {
                if ((course.getNumber() + "-" + section.getCourseSection()).equalsIgnoreCase(entry)) {
                    isBlacklist = true;
                }
            }

            return isBlacklist;
        }

        public void characters(char[] buf, int offset, int len) throws SAXException {

            String sRaw = new String(buf, offset, len);

            String s = sRaw.trim();

            if (s.length() == 0) {
                return; // ignore white space
            }

            textBuffer.append(s);
        }

        private String currentElement() {
            return "" + this.elementStack.peek();
        }

        private String readFile(String path, Charset encoding) throws IOException {
            byte[] encoded = Files.readAllBytes(Paths.get(path));
            return encoding.decode(ByteBuffer.wrap(encoded)).toString();
        }

        public void parse(String filename) throws Exception {
            // Use the default (non-validating) parser
            SAXParserFactory factory = SAXParserFactory.newInstance();

            out = new OutputStreamWriter(System.out, "UTF8");

            // Parse the input
            SAXParser saxParser = factory.newSAXParser();

            String xmlString = readFile(filename, StandardCharsets.UTF_8);

            saxParser.parse(new InputSource(new ByteArrayInputStream(xmlString.getBytes("utf-8"))), this);
        }

    }
}
