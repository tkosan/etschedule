package org.mathpiper.etschedule.model.blocks;

import com.thoughtworks.xstream.annotations.XStreamConverter;
import de.timefinder.data.CalendarSettings;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import org.joda.time.DateTime;
import org.mathpiper.etschedule.model.DaysTimesRoom;
import org.mathpiper.etschedule.output.calendar.TimeFinderPlanner;
import org.mathpiper.etschedule.utilities.SoundUtils;
import org.mathpiper.etschedule.utilities.Utility;
import org.optaplanner.core.api.domain.solution.PlanningEntityCollectionProperty;
import org.optaplanner.core.api.domain.solution.PlanningScore;
import org.optaplanner.core.api.domain.solution.PlanningSolution;
import org.optaplanner.core.api.domain.solution.drools.ProblemFactProperty;
import org.optaplanner.core.api.domain.valuerange.CountableValueRange;
import org.optaplanner.core.api.domain.valuerange.ValueRangeFactory;
import org.optaplanner.core.api.domain.valuerange.ValueRangeProvider;
import org.optaplanner.core.api.score.buildin.hardmediumsoft.HardMediumSoftScore;
import org.optaplanner.core.api.solver.Solver;
import org.optaplanner.core.api.solver.SolverFactory;
import org.optaplanner.persistence.xstream.api.score.buildin.hardmediumsoft.HardMediumSoftScoreXStreamConverter;

@PlanningSolution
public class NewBlocksSchedule {
    
    private String name;


    
    /*
    @ValueRangeProvider(id = "dayRange")
    @ProblemFactCollectionProperty
    private List<String> dayList = Arrays.asList("MWF", "TR");
    
*/
    @ValueRangeProvider(id = "startingMinuteRange")
    @ProblemFactProperty
    public CountableValueRange<Integer> startingMinuteRange() {
        return ValueRangeFactory.createIntValueRange(8*60, 18*60, 5);
    }

    
    @PlanningEntityCollectionProperty
    private List<DaysTimesRoom> daysTimesRoomList = new ArrayList<DaysTimesRoom>();

    
    @PlanningScore
    @XStreamConverter(HardMediumSoftScoreXStreamConverter.class)
    private HardMediumSoftScore score;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    
/*
    public List<String> getDayList() {
        return dayList;
    }

    public void setDayList(List<String> dayList) {
        this.dayList = dayList;
    }
*/
    
    
    
    public List<DaysTimesRoom> getDaysTimesRoomList() {
        return daysTimesRoomList;
    }

    public void setDaysTimesRoomList(List<DaysTimesRoom> daysTimesRoomList) {
        this.daysTimesRoomList = daysTimesRoomList;
    }




    public HardMediumSoftScore getScore() {
        return score;
    }

    public void setScore(HardMediumSoftScore score) {
        this.score = score;
    }

    // ************************************************************************
    // Complex methods
    // ************************************************************************
/*
    @ProblemFactCollectionProperty
    private List<CourseConflict> calculateCourseConflictList() {
        List<CourseConflict> courseConflictList = new ArrayList<>();
        for (Course leftCourse : courseList) {
            for (Course rightCourse : courseList) {
                if (leftCourse.getId() < rightCourse.getId()) {
                    int conflictCount = 0;
                    if (leftCourse.getInstructor().equals(rightCourse.getInstructor())) {
                        conflictCount++;
                    }
                    for (Curriculum curriculum : leftCourse.getCurriculumList()) {
                        if (rightCourse.getCurriculumList().contains(curriculum)) {
                            conflictCount++;
                        }
                    }
                    if (conflictCount > 0) {
                        courseConflictList.add(new CourseConflict(leftCourse, rightCourse, conflictCount));
                    }
                }
            }
        }
        return courseConflictList;
    }
    */
    
     public static void main(String[] args) throws Exception {

        // Build the Solver
        SolverFactory<NewBlocksSchedule> solverFactory = SolverFactory.createFromXmlResource(
                "org/mathpiper/etschedule/model/blocks/schedulingSolverConfig.xml");
        Solver<NewBlocksSchedule> solver = solverFactory.buildSolver();

       
        //XML xmlReader = new XML();

        //String filename = "/home/tkosan/ssu2/spring_2019/optiplanner_test_spring_2019.xml";
        //String filename = "/home/tkosan/ssu2/spring_2019/optiplanner_test_minimal_spring_2019.xml";
        //String filename = "/home/tkosan/ssu2/spring_2019/optiplanner_test_medium_spring_2019.xml";

        //xmlReader.parse(filename);

        NewBlocksSchedule baseSchedule = new NewBlocksSchedule();
        
        List dtrList = new ArrayList();
       
        // MWF 3 credit hours
        for(int x = 1; x <= 7; x++)
        {
            DaysTimesRoom dtr = new DaysTimesRoom();
            dtr.setBlockIndex(x);
            dtr.setDays("MWF");
            dtr.setStartTimeAndDurationMilitary("0800", "0855");
            dtrList.add(dtr);
        }

        
        // TR 3 credit hours
        for(int x = 1; x <= 7; x++)
        {
            DaysTimesRoom dtr = new DaysTimesRoom();
            dtr.setBlockIndex(x);
            dtr.setDays("TR");
            dtr.setStartTimeAndDurationMilitary("0800", "0915");
            dtrList.add(dtr);
        }
        
        baseSchedule.setDaysTimesRoomList(dtrList);
        
        long startTime = System.nanoTime();
        
        NewBlocksSchedule solvedSchedule = solver.solve(baseSchedule);
        
        long endTime = (System.nanoTime() - startTime) / 1000000000;
        
        //xmlReader.setDaysTimesRoomList(solvedShedule.getDaysTimeRoomList());
        

        System.out.println(solver.explainBestScore());
        // Display the result
        //xmlReader.scheduleReport();
        System.out.println("Elapsed time: " + endTime);
        //

        Utility.showSchedule(solvedSchedule.getDaysTimesRoomList(), true, 350, 70);
         
         
         SoundUtils.tone(1000,2000);

    }
}
