
package org.mathpiper.etschedule.model.blocks;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import org.mathpiper.etschedule.model.DaysTimesRoom;
import org.mathpiper.etschedule.utilities.ColorProducer;
import org.mathpiper.etschedule.utilities.Utility;


public class NewBlocksScheduleProdedural {
    
    public static void buildBlocks(List<DaysTimesRoom> dtrList, int startTime, int duration, int dayEndTime, String days, Color color) throws Exception
    {
        for(int x = 1; startTime + duration < dayEndTime; x++)
        {
            DaysTimesRoom dtr = new DaysTimesRoom();
            dtr.setBlockIndex(x);
            dtr.setDays(days);
            dtr.setStartSlot(startTime);
            dtr.setSlotsLength(duration);
            dtr.setColor(color);
            dtrList.add(dtr);
            startTime += duration + 10;
        }
    }
    
    public static void main(String[] args) throws Exception
    {
        List dtrList = new ArrayList();
        
        int dayEndTime = 16 * 60;
        
        ColorProducer colors = new ColorProducer();
       
/*      
        //Lectures
        // MWF 3 credit hours
        buildBlocks(dtrList, 8*60, 55, dayEndTime, "MWF", colors.next());
                
        
        // TR 3 credit hours
        buildBlocks(dtrList, 8*60, 80, dayEndTime, "TR", colors.next());
        
        
        // MTWR 4 credit hours
        buildBlocks(dtrList, 8*60, 55, dayEndTime, "MTWR", colors.next());
        
        
        // MWF 4 credit hours
        buildBlocks(dtrList, 8*60, 80, dayEndTime, "MWF", colors.next());

                
        // MTWR 5 credit hours
        buildBlocks(dtrList, 8*60, 55, dayEndTime, "MTWRF", colors.next());
        //*/
        
        
        //Labs
        //1500 minutes per credit hour.
        buildBlocks(dtrList, 8*60, 2*50, dayEndTime, "MTWRF", colors.next());     
        buildBlocks(dtrList, 8*60, 3*50, dayEndTime, "MTWRF", colors.next());
        buildBlocks(dtrList, 8*60, 4*50, dayEndTime, "MTWRF", colors.next());
        
        
        /*
        buildBlocks(dtrList, 8*60, 2*75, dayEndTime, "MTWRF", colors.next());     
        buildBlocks(dtrList, 8*60, 3*75, dayEndTime, "MTWRF", colors.next());
        buildBlocks(dtrList, 8*60, 4*75, dayEndTime, "MTWRF", colors.next());
*/
        
        Utility.showSchedule(dtrList, false, 350, 70);
    }
    
    
    

}
