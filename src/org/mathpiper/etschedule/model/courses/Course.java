/*
Copyright (c) 2016 Julius T. Kosan

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

package org.mathpiper.etschedule.model.courses;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Course{
    
    private String number = "";
    
    private List<Section> sections = new ArrayList<Section>();
    
    private Map<String, Section> sectionsMap = new HashMap<>();
    
    private String name;
   
  
    public Course()
    {
    }
    
    public Course(String number, String name)
    {
		super();
		
		this.number = number;
		
		this.name = name;
    }
    
    
    public void addSection(Section section)
    {
    	section.setCourse(this);
    	sections.add(section);
        
        sectionsMap.put(section.getCourseSection(), section);
    }

    
    public List<Section> getSections() {
        return sections;
    }

    
    public Section getSection(String sectionNumber)
    {
        return sectionsMap.get(sectionNumber);
    }


    public String getNumber() {
        return number;
    }


    public void setNumber(String courseNumber) {
        this.number = courseNumber;
    }


    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }
    

}