package org.mathpiper.etschedule.model.courses;

public class Instructor implements Comparable{
	private String firstName;
	private String lastName;
	private String middleInitial = "";
	private String emailAddress;
	
	
	public int compareTo(Object other) {
	    return this.getLastName().compareToIgnoreCase(((Instructor)other).getLastName());
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName.equalsIgnoreCase("")? "MISSING": firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMiddleInitial() {
		return middleInitial;
	}

	public void setMiddleInitial(String middleInitial) {
		this.middleInitial = middleInitial;
	}

	public String getEmailAddress() {
		// todo:tk:hack.
		if(emailAddress != null)
		{
			return emailAddress;
		}
		else
		{
			return this.lastName;
		}
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	
	public String toString()
	{
		return ( (firstName == null || firstName.equals("") ? "": (firstName + ". ")) + " " + (middleInitial == null || middleInitial.equals("") ? "": (middleInitial + ". ")) + lastName + " " + (this.emailAddress == null || this.emailAddress.equals("") ? "": (this.emailAddress + ". ")));
	}
	
}
