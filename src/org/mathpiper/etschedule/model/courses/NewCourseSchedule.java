package org.mathpiper.etschedule.model.courses;

import com.thoughtworks.xstream.annotations.XStreamConverter;
import java.time.Period;
import java.util.Arrays;
import java.util.List;
import org.mathpiper.etschedule.input.XML;
import org.mathpiper.etschedule.model.DaysTimesRoom;
import org.mathpiper.etschedule.utilities.SoundUtils;
import org.mathpiper.etschedule.utilities.Utility;
import org.optaplanner.core.api.domain.solution.PlanningEntityCollectionProperty;
import org.optaplanner.core.api.domain.solution.PlanningScore;
import org.optaplanner.core.api.domain.solution.PlanningSolution;
import org.optaplanner.core.api.domain.solution.drools.ProblemFactCollectionProperty;
import org.optaplanner.core.api.domain.solution.drools.ProblemFactProperty;
import org.optaplanner.core.api.domain.valuerange.CountableValueRange;
import org.optaplanner.core.api.domain.valuerange.ValueRangeFactory;
import org.optaplanner.core.api.domain.valuerange.ValueRangeProvider;
import org.optaplanner.core.api.score.buildin.hardmediumsoft.HardMediumSoftScore;
import org.optaplanner.core.api.solver.Solver;
import org.optaplanner.core.api.solver.SolverFactory;
import org.optaplanner.persistence.xstream.api.score.buildin.hardmediumsoft.HardMediumSoftScoreXStreamConverter;

@PlanningSolution
public class NewCourseSchedule {
    private String name;

    private List<Instructor> teacherList;
    private List<Course> courseList;
    private List<Section> sectionList;
    private List<Period> periodList;
    
    
    @ValueRangeProvider(id = "dayRange")
    @ProblemFactCollectionProperty
    private List<String> dayList = Arrays.asList("MW", "TR");
    
    @ValueRangeProvider(id = "locationRange")
    @ProblemFactCollectionProperty
    private List<String> locationList = Arrays.asList("ATC110","ATC154","ATC156","ATC157","ATC203","ATC204","ATC205","ATC207","ATC209","ATC253","ATC255","ATC256","ATC258","ATC301","ATC303","ATC304","ATC306","ATC309","LIB110","ONLIONLI");

    @ValueRangeProvider(id = "startingMinuteRange")
    @ProblemFactProperty
    public CountableValueRange<Integer> startingMinuteRange() {
        //return ValueRangeFactory.createIntValueRange(8*60, 18*60, 5);
        return ValueRangeFactory.createIntValueRange(8*60/5, 18*60/5, 1);
    }
    
    /*
    @ValueRangeProvider(id = "startingMinuteRange")
    @ProblemFactCollectionProperty
    private List<Integer> startingMinuteOfDayList = Arrays.asList(
        8 * 60, // 08:00
        8 * 60 + 15, // 08:15
        8 * 60 + 30, // 08:30
        8 * 60 + 45, // 08:45
        9 * 60, // 09:00
        9 * 60 + 15, // 09:15
        9 * 60 + 30, // 09:30
        9 * 60 + 45, // 09:45
        10 * 60, // 10:00
        10 * 60 + 15, // 10:15
        10 * 60 + 30, // 10:30
        10 * 60 + 45, // 10:45
        11 * 60, // 11:00
        11 * 60 + 15, // 11:15
        11 * 60 + 30, // 11:30
        11 * 60 + 45, // 11:45
        13 * 60, // 13:00
        13 * 60 + 15, // 13:15
        13 * 60 + 30, // 13:30
        13 * 60 + 45, // 13:45
        14 * 60, // 14:00
        14 * 60 + 15, // 14:15
        14 * 60 + 30, // 14:30
        14 * 60 + 45, // 14:45
        15 * 60, // 15:00
        15 * 60 + 15, // 15:15
        15 * 60 + 30, // 15:30
        15 * 60 + 45, // 15:45
        16 * 60, // 16:00
        16 * 60 + 15, // 16:15
        16 * 60 + 30, // 16:30
        16 * 60 + 45, // 16:45
        17 * 60, // 17:00
        17 * 60 + 15, // 17:15
        17 * 60 + 30, // 17:30
        17 * 60 + 45 // 17:45
        );
        */
    //private List<UnavailablePeriodPenalty> unavailablePeriodPenaltyList;

    
    @PlanningEntityCollectionProperty
    private List<DaysTimesRoom> daysTimesRoomList;

    @PlanningScore
    @XStreamConverter(HardMediumSoftScoreXStreamConverter.class)
    private HardMediumSoftScore score;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /*
    @ProblemFactCollectionProperty
    public List<Instructor> getInstructorList() {
        return teacherList;
    }

    public void setInstructorList(List<Instructor> teacherList) {
        this.teacherList = teacherList;
    }
    */


    /*
    @ProblemFactCollectionProperty
    public List<Course> getCourseList() {
        return courseList;
    }

    public void setCourseList(List<Course> courseList) {
        this.courseList = courseList;
    }
    */
    

    public List<String> getLocationList() {
        return locationList;
    }

    public void setLocationList(List<String> roomList) {
        this.locationList = roomList;
    }
    
    
    

    public List<String> getDayList() {
        return dayList;
    }

    public void setDayList(List<String> dayList) {
        this.dayList = dayList;
    }

    
/*
    public List<Integer> getStartingMinuteOfDayList() {
        return startingMinuteOfDayList;
    }

    public void setStartingMinuteOfDayList(List<Integer> startingMinuteOfDayList) {
        this.startingMinuteOfDayList = startingMinuteOfDayList;
    }
*/
    
    public List<DaysTimesRoom> getDaysTimesRoomList() {
        return daysTimesRoomList;
    }

    public void setDaysTimesRoomList(List<DaysTimesRoom> daysTimeRoomList) {
        this.daysTimesRoomList = daysTimeRoomList;
    }

    /*
    @ValueRangeProvider(id = "periodRange")
    @ProblemFactCollectionProperty
    public List<Period> getPeriodList() {
        return periodList;
    }

    public void setPeriodList(List<Period> periodList) {
        this.periodList = periodList;
    }
    */
    
    
    
    /*
    @ProblemFactCollectionProperty
    public List<UnavailablePeriodPenalty> getUnavailablePeriodPenaltyList() {
        return unavailablePeriodPenaltyList;
    }

    public void setUnavailablePeriodPenaltyList(List<UnavailablePeriodPenalty> unavailablePeriodPenaltyList) {
        this.unavailablePeriodPenaltyList = unavailablePeriodPenaltyList;
    }
*/
    
    
    /*
    @PlanningEntityCollectionProperty
    public List<Section> getSectionList() {
        return sectionList;
    }

    public void setSectionList(List<Section> lectureList) {
        this.sectionList = lectureList;
    }
    */


    public HardMediumSoftScore getScore() {
        return score;
    }

    public void setScore(HardMediumSoftScore score) {
        this.score = score;
    }

    // ************************************************************************
    // Complex methods
    // ************************************************************************
/*
    @ProblemFactCollectionProperty
    private List<CourseConflict> calculateCourseConflictList() {
        List<CourseConflict> courseConflictList = new ArrayList<>();
        for (Course leftCourse : courseList) {
            for (Course rightCourse : courseList) {
                if (leftCourse.getId() < rightCourse.getId()) {
                    int conflictCount = 0;
                    if (leftCourse.getInstructor().equals(rightCourse.getInstructor())) {
                        conflictCount++;
                    }
                    for (Curriculum curriculum : leftCourse.getCurriculumList()) {
                        if (rightCourse.getCurriculumList().contains(curriculum)) {
                            conflictCount++;
                        }
                    }
                    if (conflictCount > 0) {
                        courseConflictList.add(new CourseConflict(leftCourse, rightCourse, conflictCount));
                    }
                }
            }
        }
        return courseConflictList;
    }
    */
    
     public static void main(String[] args) throws Exception {

        // Build the Solver
        SolverFactory<NewCourseSchedule> solverFactory = SolverFactory.createFromXmlResource(
                "org/mathpiper/etschedule/model/courses/schedulingSolverConfig.xml");
        Solver<NewCourseSchedule> solver = solverFactory.buildSolver();

        // Load a problem with 400 computers and 1200 processes
        //CloudBalance unsolvedCloudBalance = new CloudBalancingGenerator().createCloudBalance(400, 1200);
                
        XML xmlReader = new XML();

        //String filename = "/home/tkosan/ssu2/spring_2019/optiplanner_test_spring_2019.xml";
        //String filename = "/home/tkosan/ssu2/spring_2019/optiplanner_test_minimal_spring_2019.xml";
        //String filename = "/home/tkosan/ssu2/spring_2019/optiplanner_test_medium_spring_2019.xml";
        String filename = "/home/tkosan/ssu2/fall_2019/cet_em_only_optiplanner_fall_2019.xml";

        xmlReader.parse(filename);
        
        
        
        NewCourseSchedule baseSchedule = new NewCourseSchedule();
        
        baseSchedule.setDaysTimesRoomList(xmlReader.getDaysTimesRoomList());
        NewCourseSchedule solvedSchedule = solver.solve(baseSchedule);
        xmlReader.setDaysTimesRoomList(solvedSchedule.getDaysTimesRoomList());
        

        // Display the result
        //xmlReader.scheduleReport();
        
        Utility.showSchedule(solvedSchedule.getDaysTimesRoomList(), true, 400, 70);
         
         
         SoundUtils.tone(1000,2000);

    }
}
