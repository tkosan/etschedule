/*
Copyright (c) 2016 Julius T. Kosan

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

package org.mathpiper.etschedule.model.courses;

import org.mathpiper.etschedule.model.courses.Course;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import org.mathpiper.etschedule.model.DaysTimesRoom;

public class Section {

	private Course course;
	private String courseSection;
	private String instructors = null;
	private Instructor instructorOfRecord;
	private int seatsOpen = -1;
	private int capacity = -1;
	private String status;
	private List<DaysTimesRoom> daysTimesRoomList = new ArrayList<DaysTimesRoom>();
	private LocalDate startDate;
	private LocalDate endDate;
	private int lectureHours = -1; //todo:tk.
	private int labHours = -1; //todo:tk/
	private BigDecimal creditHours;
	private boolean isHonors = false;

	public Section() {
		super();
	}

	public void addDaysTimesRoom(DaysTimesRoom daysTimesRoom) {
		daysTimesRoomList.add(daysTimesRoom);
	}
        
	public List<DaysTimesRoom> getDaysTimesRoomList() {
		return daysTimesRoomList;
	}

	public void setDaysTimesRoomList(List daysAndTimesList) {
		this.daysTimesRoomList = daysAndTimesList;
	}
        
        public void clearDaysTimesRoomList()
        {
            this.daysTimesRoomList.clear();
        }

	public String getCourseNumber() {
		return course.getNumber();
	}

	public String getCourseSection() {
		return courseSection;
	}

	public void setCourseSection(String courseSection) {
		this.courseSection = courseSection;
	}

	public String getCourseName() {
		return course.getName();
	}

	public String getInstructors() {
		return instructors;
	}

	public void setInstructors(String instructors) {
		this.instructors = instructors;
	}

	public int getSeatsOpen() {
		return seatsOpen;
	}

	public void setSeatsOpen(int seatsOpen) {
		this.seatsOpen = seatsOpen;
	}

	public int getCapacity() {
		return capacity;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public LocalDate getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}

	public LocalDate getEndDate() {
		return endDate;
	}

	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}
        
        public long getWeeks()
        {
            //The +1 adjusts for the end date being specified at the
            //end of a work week.
            return Math.abs(ChronoUnit.WEEKS.between(startDate, endDate) + 1);
        }

	public BigDecimal getCreditHours() {
		return creditHours;
	}

	public void setCreditHours(BigDecimal creditHours) {
		this.creditHours = creditHours;
	}
	
	public boolean getIsHonors() {
		return isHonors;
	}

	public void setIsHonors(boolean isHonors) {
		this.isHonors = isHonors;
	}
	

	public Instructor getInstructorOfRecord() {
		// todo:tk:hack.
		if(instructorOfRecord != null)
		{
			return instructorOfRecord;
		}
		else
		{
			Instructor instructor = new Instructor();
			instructor.setLastName(this.instructors);
			return instructor;
		}
	}

	public void setInstructorOfRecord(Instructor instructorOfRecord) {
		this.instructorOfRecord = instructorOfRecord;
	}
	
	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}
	
	public int getLectureHours() {
		return lectureHours;
	}

	public void setLectureHours(int lectureHours) {
		this.lectureHours = lectureHours;
	}

	public int getLabHours() {
		return labHours;
	}

	public void setLabHours(int labHours) {
		this.labHours = labHours;
	}

	public void setHonors(boolean isHonors) {
		this.isHonors = isHonors;
	}
        
        public long getMinutesPerWeek()
        {
            long minutesPerWeek = 0;
            
            for(DaysTimesRoom dtr:this.daysTimesRoomList)
            {
                minutesPerWeek += dtr.getMinutesPerWeek();
            }
            
            return minutesPerWeek;
        }
        
        public long getMinutesPerSection()
        {
            long weeks = this.getWeeks();
            return getMinutesPerWeek() * weeks;
        }

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();

		sb.append(this.course.getNumber());
		sb.append("|");

		sb.append(this.courseSection);
		sb.append("|");

		sb.append(this.course.getName());
		sb.append("|");

		sb.append(this.getInstructorOfRecord().toString().trim());
		sb.append("|");

		sb.append(this.lectureHours);
		sb.append("|");
		
		sb.append(this.labHours);
		sb.append("|");
		
		sb.append(this.creditHours);
		sb.append("|");

		sb.append(this.seatsOpen);
		sb.append("|");

		sb.append(this.capacity);
		sb.append("|");

		for (DaysTimesRoom s : daysTimesRoomList) {
			sb.append(s.toString());
			sb.append(", ");
		}

		return sb.toString();

	}

}