/*
 *  Copyright 2009 Peter Karich.
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */
package org.mathpiper.etschedule.output.calendar;

import java.awt.Insets;
import javax.swing.JButton;

/**
 * @author Peter Karich
 */
public class SmallButton extends JButton {

    public SmallButton(String str) {
        super(str);
    }

    @Override
    public Insets getInsets() {
        return new Insets(2, 2, 2, 2);
    }
}
