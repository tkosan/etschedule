package org.mathpiper.etschedule.query;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.mathpiper.etschedule.input.SemesterSchedule;
import org.mathpiper.etschedule.input.XML;
import org.mathpiper.etschedule.model.DaysTimesRoom;
import org.mathpiper.etschedule.model.courses.Section;
import org.mathpiper.etschedule.utilities.Utility;

public class DayAndTimeOverlap {

    private int lineNumber = 0;

    private List<DaysTimesRoom> daysAndTimes = new ArrayList<DaysTimesRoom>();

    private List<DaysTimesRoom> openBlocksList = new ArrayList<DaysTimesRoom>();

    public void loadTimeBlocks(File file) throws Exception {

        if (!file.exists()) {
            throw new IOException(file.getName() + " does not exist.");

        }

        if (!(file.isFile() && file.canRead())) {
            throw new IOException(file.getName() + " cannot be read from.");
        }

        BufferedReader fis = null;
        try {
            fis = new BufferedReader(new FileReader(file));
            char character = ' ';
            String row;

            while ((row = fis.readLine()) != null) {
                lineNumber++;

                String rowString = row.toString().trim();

                DaysTimesRoom daysTimesRoom = SemesterSchedule.createDaysTimesRoom(rowString);

                daysAndTimes.add(daysTimesRoom);
            }

            System.out.println();

        } catch (Exception e) {
            System.err.println("Line Number: " + lineNumber);
            e.printStackTrace();
        } finally {

            if (fis != null) {
                fis.close();
            }
        }
    }// end method

    public void loadTimeBlocks(List<Section> sections) {
        for (Section section : sections) {
            daysAndTimes.addAll(section.getDaysTimesRoomList());
        }
    }

    public void loadTimeBlocks(Section section) {
        daysAndTimes.addAll(section.getDaysTimesRoomList());
    }



    public void findOpenTimeBlocks(String days, int timeIncrement) throws Exception {
        
        if(daysAndTimes.size() == 0)
        {
            throw new Exception("No day and time data has been loaded.");
        }

        DaysTimesRoom timeBlockScan = new DaysTimesRoom();

        //String days = "TW";
        //int timeIncrement = 5;
        timeBlockScan.setDays(days);

        //timeBlockScan.setStartTimeAndDuration("8:00", "AM", "12:30", "PM"); // 4.5 hour time block.
        //timeBlockScan.setStartTimeAndDuration("8:00", "AM", "9:00", "AM"); // One hour time block.
        //timeBlockScan.setStartTimeAndDuration("8:00", "AM", "10:15", "AM"); // 2 hour and 15 minute time block.
        timeBlockScan.setStartTimeAndDuration("8:00", "AM", "8:30", "AM"); // 2 hour time block.
        //timeBlockScan.setStartTimeAndDuration("12:00", "PM", "2:00", "PM"); // 2 hour time block.
        //timeBlockScan.setStartTimeAndDuration("8:00", "AM", "8:50", "AM"); // 50 minute time block
        //timeBlockScan.setStartTimeAndDuration("8:00", "AM", "8:45", "AM"); // 45 minute time block
        //timeBlockScan.setStartTimeAndDuration("8:00", "AM", "8:30", "AM"); // 30 minute time block
        //timeBlockScan.setStartTimeAndDuration("8:00", "AM", "8:25", "AM"); // 45 minute time block

//		while(!timeBlockScan.isEndTime("10:00", "PM") && !timeBlockScan.isEndTime("10:05", "PM"))
        while (!timeBlockScan.isPastEndTime("7:00", "PM")) {
            boolean overlap = false;

            for (DaysTimesRoom timeBlock : daysAndTimes) {
                if (!timeBlockScan.isDaysOverlap(timeBlock)) {
                    continue;
                }

                if (timeBlockScan.isTimeOverlap(timeBlock)) {
                    overlap = true;
                    break;
                }
            }

            if (!overlap) {
                System.out.println(timeBlockScan.toString());
            }

            if (timeBlockScan.increaseTime(timeIncrement)) {
                //System.out.println("AM -> PM");
            }

        }

    }

    public List<DaysTimesRoom> getDaysAndTimes() {
        return daysAndTimes;
    }

    public void setDaysAndTimes(List<DaysTimesRoom> daysAndTimes) {
        this.daysAndTimes = daysAndTimes;
    }

    public static void main(String[] args) throws Exception {

        /*
            File dayAndTimeFile = new File("/home/tkosan/ssu2/algorithms_section_2.txt/days_and_times.txt");

            DayAndTimeOverlap overlap = new DayAndTimeOverlap();

            try {
                    overlap.loadTimeBlocks(dayAndTimeFile);

                    overlap.findOpenTimeBlocks("TW", 5);

            } catch (Exception e) {
                    e.printStackTrace();
            }
		
         //*/
        
        
        XML xmlReader = new XML();

        String filename = "/home/tkosan/ssu2/uis/courses_1_3_2019_2_06_pm.xml";
        //String filename = "/home/tkosan/ssu2/spring_2019/optiplanner_test_spring_2019.xml";

        xmlReader.parse(filename);

        List dtrList = xmlReader.getDaysTimesRoomList(new String[]{"ARTH1101-01", "ARTH1101-02", "ARTH3366-02", "ARTS1101-03", "ARTS1103-51", "BIOL1130-01", "BIOL1130-05", "BIOL1130-06", "BIOL1130-51", "BIOL1130-52", "BUIS1010-02", "BUIS1010-04", "BUIS1010-06", "BUIS1010-51", "BUIS2200-01", "BULW2700-51", "BUOA1150-01", "CHEM1121-04", "COMM1103-06", "EDUC1115-04", "ENGL1101-08", "ENGL1101-16", "ENGL1101-17", "ENGL1101-21", "ENGL1101-90", "ENGL1105-05", "ENGL1105-08", "ENGL1105-09", "ENGL1105-20", "ENGL1999-17", "ENGL2200-02", "ETEM1111-01", "ETEM1216-01", "HIST3432-01", "HUMA2225-02", "HUMA2227-02", "MATH0101-01", "MATH0102-01", "MATH0120A-01", "MATH1000-91", "NTSC1110-03", "POLS1310-01", "POLS1410-01", "PSCI2251-51", "PSYC1101-51", "PSYC1101-52", "PSYC1101-53", "PSYC2170-51", "SIGN1010-51", "SOCI1101-02", "SOCI1101-03", "SSES2100-01", "SSPE3600-51", "STAT1150-02", "STAT1150-04", "THAR1000-02", "THAR1000-06", "THAR1000-07", "UNIV1101-3A"});
        
        DayAndTimeOverlap overlap = new DayAndTimeOverlap();
        
        overlap.setDaysAndTimes(dtrList);
        
        System.out.println("Monday");
        overlap.findOpenTimeBlocks("M", 5);
        
        System.out.println("\nTuesday");
        overlap.findOpenTimeBlocks("T", 5);
        
        System.out.println("\nWednesday");
        overlap.findOpenTimeBlocks("W", 5);
        
        System.out.println("\nThursday");
        overlap.findOpenTimeBlocks("R", 5);

        Utility.showSchedule(dtrList, true, 350, 70);
    }

}
