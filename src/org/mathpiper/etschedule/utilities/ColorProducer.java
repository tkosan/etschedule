
package org.mathpiper.etschedule.utilities;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Random;


public class ColorProducer {
    
    private ArrayList<Color> colors = new ArrayList<>();
    
    private Random random = new Random();

    private int colorIndex = 0;
    
    public ColorProducer()
    {
        colors.add(new Color(207, 69, 35)); 
        colors.add(new Color(69, 207, 35)); 
        colors.add(new Color(16, 120, 108)); 
        colors.add(new Color(19, 156, 207)); 
        colors.add(new Color(224, 13, 154)); 
        colors.add(new Color(207, 69, 35)); 
        colors.add(new Color(32, 94, 17)); 
        colors.add(new Color(161, 10, 62)); 
        colors.add(new Color(18, 87, 224)); 
        colors.add(new Color(207, 132, 35)); 
        colors.add(new Color(23, 212, 139)); 
        colors.add(new Color(57, 5, 168)); 
        colors.add(new Color(222, 100, 118)); 
        colors.add(new Color(207, 198, 35)); 
        colors.add(new Color(30, 217, 195)); 
        colors.add(new Color(217, 147, 137)); 
        colors.add(new Color(147, 207, 35)); 
        colors.add(new Color(217, 185, 137)); 
        colors.add(new Color(171, 39, 232)); 
    }
    
    public Color next()
    {
        if(colorIndex == colors.size())
        {
            //colorIndex = 0;
            return new Color(random.nextInt(255), random.nextInt(255), random.nextInt(255));
        }
        else
        {
            return colors.get(colorIndex++);
        }
    }
}
