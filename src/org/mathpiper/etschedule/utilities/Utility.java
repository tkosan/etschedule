package org.mathpiper.etschedule.utilities;

import de.timefinder.data.CalendarSettings;
import de.timefinder.data.IntervalLong;
import de.timefinder.data.IntervalLongImpl;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.List;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.SwingUtilities;
import org.joda.time.DateTime;
import org.mathpiper.etschedule.model.DaysTimesRoom;
import org.mathpiper.etschedule.output.calendar.TimeFinderPlanner;

public class Utility {

    public static void showSchedule(List<DaysTimesRoom> daysTimesRoomList, boolean isBorders, int dayWidth, int hourHeight) {
        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                JFrame frame = new JFrame("Block Schedule");
                frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

                CalendarSettings settings = new CalendarSettings();

                settings.setMillisPerTimeslot(5 * 60 * 1000L);

                settings.setTimeslotsPerDay(13 * 12); // 12 5 minute time slots per hour.

                settings.setNumberOfDays(5);

                //DateTime(int year, int monthOfYear, int dayOfMonth, int hourOfDay, int minuteOfHour, int secondOfMinute, int millisOfSecond)
                settings.setStartDate(new DateTime(2018, 10, 1, 8, 0, 0, 0));
                TimeFinderPlanner planner = new TimeFinderPlanner(settings, dayWidth, hourHeight);

                for (DaysTimesRoom daysTimesRoom : daysTimesRoomList) {

                    String days = daysTimesRoom.daysString();

                    for (char day : days.toCharArray()) {
                        int dayIndex = 0;

                        if (day == 'M') {
                            dayIndex = 1;
                        } else if (day == 'T') {
                            dayIndex = 2;
                        } else if (day == 'W') {
                            dayIndex = 3;
                        } else if (day == 'R') {
                            dayIndex = 4;
                        } else if (day == 'F') {
                            dayIndex = 5;
                        } else if (day == 'S') {
                            dayIndex = 6;
                        } else if (day == 'U') {
                            dayIndex = 7;
                        }

                        if (dayIndex != 0) { 
                            //System.out.println(daysTimesRoom.toString()+ ", " + daysTimesRoom.startTimeHours() + ", " +  daysTimesRoom.startTimeMinutesIntoHour()+ ", " +  daysTimesRoom.durationMinutes());
                            // String description, int year, int month, int day, int hour, int minute, int durationInMinutes
                            IntervalLong interval = new IntervalLongImpl(daysTimesRoom.getScheduleText(), 2018, 10, dayIndex, daysTimesRoom.startTimeHours(), daysTimesRoom.startTimeMinutesIntoHour(), daysTimesRoom.durationMinutes());
                            interval.setColor(daysTimesRoom.getColor());
                            interval.setIsBorder(isBorders);
                            planner.addInterval(interval);
                        }
                    }
                }
                
                JMenu fileMenu = new JMenu("File");
                JMenuItem saveAsImageAction = new JMenuItem();
                saveAsImageAction.setText("Save As Image");
                saveAsImageAction.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent ae) {
                        Utility.saveImageOfComponent(planner);
                    }
                });

                fileMenu.add(saveAsImageAction);
                JMenuBar menuBar = new JMenuBar();
                menuBar.add(fileMenu);
                frame.setJMenuBar(menuBar);

                frame.setContentPane(planner);
                frame.setSize(1000, 640);
                frame.setVisible(true);
            }
        }
        );

    }

    public static void saveImageOfComponent(JComponent component) {
        JFileChooser saveImageFileChooser = new JFileChooser();

        int returnValue = saveImageFileChooser.showSaveDialog(component);

        if (returnValue == JFileChooser.APPROVE_OPTION) {
            File exportImageFile = saveImageFileChooser.getSelectedFile();
            try {
                ScreenCapture.createImage(component, exportImageFile.getAbsolutePath());
            } catch (java.io.IOException ioe) {
                ioe.printStackTrace();
            }//end try/catch.

        }
    }

}
