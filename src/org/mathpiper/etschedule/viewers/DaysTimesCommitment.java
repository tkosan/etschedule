/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mathpiper.etschedule.viewers;

/**
 *
 * @author jakosan
 */
import java.awt.Color;
import org.mathpiper.etschedule.model.courses.Section;
import org.optaplanner.core.api.domain.entity.PlanningEntity;
import org.optaplanner.core.api.domain.variable.PlanningVariable;

@PlanningEntity
public class DaysTimesCommitment {

	public static int dayBitPositions[] = new int[] { 64, 32, 16, 8, 4, 2, 1 };

	public static String dayIdentifiers[] = new String[] { "M", "T", "W", "R", "F", "S", "U" };
	private int hashCode;
        
        private Section parentSection;
        
        private int blockIndex = 0; // Used for creating block schedules.
        
        private int timeGrain = 1; // Minutes.
        

	private String commitmentType = "";
        
        private Color color;
        
	// Units are timeGrain time blocks
	private int slotsLength;
        
        // ==== Planning variables: changes during planning, between score calculations.
        
	// Units are timeGrain time blocks starting at 12:00 AM.
	// The first slot is slot 0.
	private int startSlot;
        
	// MWF = 1010100.
	private int daysPattern = 0;
	
	public DaysTimesCommitment()
	{
	}

	public DaysTimesCommitment(String days, String startTimeString, String startTimeMeridianString, String endTimeString,
			String endTimeMeridianString, String commitmentType) throws Exception {

		daysPattern = daysToDaysPattern(days);

		setStartTimeAndDuration(startTimeString, startTimeMeridianString, endTimeString, endTimeMeridianString);

		this.commitmentType = commitmentType;

		

		hashCode = combineHash(combineHash(daysPattern, startSlot), slotsLength);

	}

	public int daysToDaysPattern(String days) throws Exception {

            int newDaysPattern = 0;

            if (days.contains("M")) {
                    newDaysPattern = newDaysPattern | Integer.parseInt("1000000", 2);
            }
            if (days.contains("T")) {
                    newDaysPattern = newDaysPattern | Integer.parseInt("0100000", 2);
            }
            if (days.contains("W")) {
                    newDaysPattern = newDaysPattern | Integer.parseInt("0010000", 2);
            }
            if (days.contains("R")) {
                    newDaysPattern = newDaysPattern | Integer.parseInt("0001000", 2);
            }
            if (days.contains("F")) {
                    newDaysPattern = newDaysPattern | Integer.parseInt("0000100", 2);
            }
            if (days.contains("S")) {
                    newDaysPattern = newDaysPattern | Integer.parseInt("0000010", 2);
            }
            if (days.contains("U")) {
                    newDaysPattern = newDaysPattern | Integer.parseInt("0000001", 2);
            }
            
            if(newDaysPattern == 0 && ! days.equals("-------"))
            {
                throw new Exception("The argument contains an invalid days pattern.");
            }

            return newDaysPattern;
	}
        
	public String patternToDays(int daysPattern) throws Exception {

            StringBuilder sb = new StringBuilder();
            
            if ((daysPattern & Integer.parseInt("1000000", 2)) != 0) {
                sb.append("M");
            }
            if ((daysPattern & Integer.parseInt("0100000", 2)) != 0) {
                sb.append("T");
            }
            if ((daysPattern & Integer.parseInt("0010000", 2)) != 0) {
                sb.append("W");
            }
            if ((daysPattern & Integer.parseInt("0001000", 2)) != 0) {
                sb.append("R");
            }
            if ((daysPattern & Integer.parseInt("0000100", 2)) != 0) {
                sb.append("F");
            }
            if ((daysPattern & Integer.parseInt("0000010", 2)) != 0) {
                sb.append("S");
            }
            if ((daysPattern & Integer.parseInt("0000001", 2)) != 0) {
                sb.append("U");
            }
            
            if(sb.length() == 0)
            {
                throw new Exception("The argument contains an invalid day pattern.");
            }

            return sb.toString();
	}
        
        
        public Integer getDaysPattern() {
            return daysPattern;
	}
        
        public void setDaysPattern(Integer daysPattern)
        {
            this.daysPattern = daysPattern;
        }
	


	public void setStartTimeAndDuration(String startTimeString, String startTimeMeridianString, String endTimeString,
			String endTimeMeridianString) throws Exception {

		// Start time.
		String[] startTimeHourAndMinute = startTimeString.split(":");

		int startTimeHour = Integer.parseInt(startTimeHourAndMinute[0]);

		int startTimeMinute = Integer.parseInt(startTimeHourAndMinute[1]);

		startSlot = startTimeHour * (60/timeGrain) /* slots per hour */ + startTimeMinute / timeGrain;

		if (startTimeMeridianString.contains("PM") && startTimeHour != 12) {
			startSlot = startSlot + (1440/timeGrain)/2 /* slots in 1/2 day */;
		}

		// End time.
		String[] endTimeHourAndMinute = endTimeString.split(":");

		int endTimeHour = Integer.parseInt(endTimeHourAndMinute[0]);

		int endTimeMinute = Integer.parseInt(endTimeHourAndMinute[1]);

		int endSlot = endTimeHour * (60/timeGrain) /* slots per hour */ + endTimeMinute / timeGrain;

		if (endTimeMeridianString.contains("PM") && endTimeHour != 12) {
			endSlot = endSlot + (1440/timeGrain)/2  /* slots in 1/2 day */;
		}

		slotsLength = endSlot - startSlot;
	}
	
	
	public void setStartTimeAndDurationMilitary(String startTimeString, String endTimeString) throws Exception
	{
		if(startTimeString.length() != 4)
		{
			throw new Exception("The start time string length is not correct.");
		}
		
		// Start time.
		int startTimeHour = Integer.parseInt(startTimeString.substring(0,2));
	
		int startTimeMinute = Integer.parseInt(startTimeString.substring(2,4));
		
		startSlot = startTimeHour * (60/timeGrain) /* slots per hour */ + startTimeMinute / timeGrain;
		
		
		if(endTimeString.length() != 4)
		{
			throw new Exception("The end time string length is not correct.");
		}
		
		// End time.
		int endTimeHour = Integer.parseInt(endTimeString.substring(0,2));
	
		int endTimeMinute = Integer.parseInt(endTimeString.substring(2,4));
		
		int endSlot = endTimeHour * (60/timeGrain) /* slots per hour */ + endTimeMinute / timeGrain;
		
		slotsLength = endSlot - startSlot;
	}
        
        
        /*
        LocalTime getStartLocalTime()
        {
            return LocalTime.ofSecondOfDay(this.startSlot * this.timeGrain * 60);
        }
        */

	
	public boolean isDaysOverlap(DaysTimesCommitment other) {
		return ((daysPattern & other.daysPattern) != 0);
	}

	public boolean isTimeOverlap(DaysTimesCommitment other) {
		return (startSlot + slotsLength > other.startSlot) && (other.startSlot + other.slotsLength > startSlot);
	}
	
	
	public boolean isPastEndTime(String timeString, String meridanString)
	{
		String[] timeHourAndMinute = timeString.split(":");

		int timeHour = Integer.parseInt(timeHourAndMinute[0]);

		int timeMinute = Integer.parseInt(timeHourAndMinute[1]);
		int slot = timeHour * (60/timeGrain) /* slots per hour */ + timeMinute / timeGrain;

		if (meridanString.contains("PM") && timeHour != 12) {
			slot = slot + 720/timeGrain /* slots in 1/2 day */;
		}
		
		return startSlot + slotsLength > slot;
	}
	
	public boolean increaseTime(int incrementMinutes)
	{
		int incrementSlots = incrementMinutes/timeGrain;
		int oldSlot = startSlot;
		startSlot += incrementSlots;
		
		if(oldSlot <= (1440/timeGrain)/2 && startSlot > (1440/timeGrain)/2)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public boolean hoursIdentical(DaysTimesCommitment other) {
		return (startSlot == other.startSlot) && (slotsLength == other.slotsLength);
	}
        
        
        //=================================================================
        //@PlanningVariable(valueRangeProviderRefs = {"dayRange"}) todo:tk
        public String getDays() throws Exception {
            return this.patternToDays(daysPattern);
        }
        
	public void setDays(String days) throws Exception
	{
		daysPattern = daysToDaysPattern(days);
	}
        


        //=================================================================
        @PlanningVariable(valueRangeProviderRefs = {"startingMinuteRange"})
	public Integer getStartSlot() {
		return startSlot;
	}
        
        public void setStartSlot(Integer startSlot) throws Exception
        {
            if(startSlot < 0 || startSlot > 1440/timeGrain)
            {
                throw new Exception("Invalid start slot value: " + startSlot);
            }
            
            this.startSlot = startSlot;
        }

        
	public int getSlotsLength() {
		return slotsLength;
	}
        
        public void setSlotsLength(int slotsLength) throws Exception
        {
            if(slotsLength < 0 || slotsLength > 1440/timeGrain)
            {
                throw new Exception("Invalid slots length value: " + slotsLength);
            }
            
            this.slotsLength = slotsLength;
        }
        
	public long getMinutesPerWeek() {
            int totalDays = 0;
            
            int daysPatternCopy = daysPattern;
            
            for(int x = 0; x < 7; x++)
            {
                if((daysPatternCopy & 1) == 1)
                {
                    totalDays++;
                }
                daysPatternCopy = daysPatternCopy >> 1;
            }
            
            return (slotsLength * this.timeGrain) * totalDays;
	}

	private static int combineHash(int a, int b) {
		int ret = 0;
		for (int i = 0; i < 15; i++) //todo:tk:what is the meaning of "15" here?
			ret = ret | ((a & (1 << i)) << i) | ((b & (1 << i)) << (i + 1));
		return ret;
	}

	public String getCommitmentType() {
		return commitmentType;
	}

	public void setCommitmentType(String commitmentType) {
		this.commitmentType = commitmentType;
	}

        public int getBlockIndex() {
            return blockIndex;
        }

        public void setBlockIndex(int blockIndex) {
            this.blockIndex = blockIndex;
        }
        
        public String getInstructorEmailAddress()
        {
            return parentSection.getInstructorOfRecord().getEmailAddress();
        }
        

	


	// =============== Printable versions of days and times.
	public String daysString() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 7; i++) {
            if ((daysPattern & dayBitPositions[i]) != 0) {
                sb.append(dayIdentifiers[i]);
            } else {
                sb.append("-");
            }
        }

        return sb.toString();
    }

	public String startTimeString() {
		int min = startSlot * timeGrain;
		int h = min / 60;
		int m = min % 60;
		return (h > 12 ? h - 12 : h) + ":" + (m < 10 ? "0" : "") + m + (h >= 12 ? "PM" : "am");
	}
        
	public String startTimeMilitaryString() {
		int min = startSlot * timeGrain;
		int h = min / 60;
		int m = min % 60;
		return (h < 10 ? "0" : "") + h + (m < 10 ? "0" : "") + m;
	}
        
        public int startTimeHours()
        {
            int min = startSlot * timeGrain;
            return min / 60;
        }
        
        public int startTimeMinutesIntoHour()
        {
            int min = startSlot * timeGrain;
            return min % 60;
        }

	public String endTimeString() {
		int min = (startSlot + slotsLength) * timeGrain;
		int m = min % 60;
		int h = min / 60;
		return (h > 12 ? h - 12 : h) + ":" + (m < 10 ? "0" : "") + m + (h >= 12 ? "PM" : "am");
	}
        
	public String endTimeMilitaryString() {
		int min = (startSlot + slotsLength) * timeGrain;
		int m = min % 60;
		int h = min / 60;
		return (h < 10 ? "0" : "") + h + (m < 10 ? "0" : "") + m;
	}
        
        public int durationMinutes()
        {
            return slotsLength * timeGrain;
        }


	// ============================

	@Override
	public boolean equals(Object o) {
		if (o == null || !(o instanceof DaysTimesCommitment))
			return false;
		DaysTimesCommitment other = (DaysTimesCommitment) o;
		if (startSlot != other.startSlot)
			return false;
		if (slotsLength != other.slotsLength)
			return false;
		if (daysPattern != other.daysPattern)
			return false;
		return true;
	}

        public Color getColor() {
            return color;
        }

        public void setColor(Color color) {
            this.color = color;
        }
        
        
	@Override
	public int hashCode() {
		return hashCode;
	}

	@Override
	public String toString() {
		String resultString =  /*blockIndex + "\n" +*/ 
                        ((commitmentType != null) ? commitmentType : "") + 
                        "\n" + daysString() + "\n" + startTimeString() + "-\n" + endTimeString() + "\n";
                return resultString.trim();
	}
        
        public static void main(String[] args) throws Exception
        {
            DaysTimesCommitment dtr = new DaysTimesCommitment();
            dtr.setStartTimeAndDurationMilitary("1500", "1708");
            
            System.out.println(dtr.startTimeHours() + ", " + dtr.startTimeMinutesIntoHour());
        }

}
