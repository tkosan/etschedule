/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mathpiper.etschedule.viewers;

import de.timefinder.data.CalendarSettings;
import de.timefinder.data.IntervalLong;
import de.timefinder.data.IntervalLongImpl;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import org.joda.time.DateTime;
import org.mathpiper.etschedule.output.calendar.TimeFinderPlanner;
import org.mathpiper.etschedule.utilities.ScreenCapture;
import org.mathpiper.etschedule.utilities.ColorProducer;


/**
 *
 * @author jakosan
 */
public class ScheduleMaker {
    
    private int numberOfDaysShown = 7;
    private boolean isBorders = true;
    private int daysWidth = 200;
    private int hoursHeight = 100;
    
    private JTextField numberOfdaysShownField = new JTextField(5);
    private JTextField daysWidthField = new JTextField(5);
    private JTextField hoursHeightField = new JTextField(5);
    private JTextField fileNameField = new JTextField(20);
    
    private List<DaysTimesCommitment> daysTimesCommitmentList;
    
    private File scheduleFile;
    
    public static DaysTimesCommitment createDaysTimesCommitment(String row) throws Exception {
        
        String[] dateAndCommitment = row.split("[\"]+");
        
        String[] daysAndTimesWithoutBothMeridians = dateAndCommitment[0].split("[ -]+");
        
        String commitmentType = dateAndCommitment[1];
        
        int colorNumber = Integer.parseInt(dateAndCommitment[2].strip());
                
        ColorProducer colorProducer = new ColorProducer();
        
        String[] daysAndTimesWithBothMeridians = new String[6];

        daysAndTimesWithBothMeridians[0] = daysAndTimesWithoutBothMeridians[0];

        // Handle case where class spans AM and PM.
        if (daysAndTimesWithoutBothMeridians[1].endsWith("M")) {
            String timePart = daysAndTimesWithoutBothMeridians[1].substring(0,
                    daysAndTimesWithoutBothMeridians[1].length() - 2);

            String meridianPart = daysAndTimesWithoutBothMeridians[1].substring(
                    daysAndTimesWithoutBothMeridians[1].length() - 2, daysAndTimesWithoutBothMeridians[1].length());

            daysAndTimesWithBothMeridians[1] = timePart;
            daysAndTimesWithBothMeridians[2] = meridianPart;
            daysAndTimesWithBothMeridians[3] = daysAndTimesWithoutBothMeridians[2];
            daysAndTimesWithBothMeridians[4] = daysAndTimesWithoutBothMeridians[3];
        } else {
            daysAndTimesWithBothMeridians[1] = daysAndTimesWithoutBothMeridians[1];
            daysAndTimesWithBothMeridians[2] = new String(daysAndTimesWithoutBothMeridians[3]);
            daysAndTimesWithBothMeridians[3] = daysAndTimesWithoutBothMeridians[2];
            daysAndTimesWithBothMeridians[4] = daysAndTimesWithoutBothMeridians[3];
        }

        String days = daysAndTimesWithBothMeridians[0];
        String startTimeString = daysAndTimesWithBothMeridians[1];
        String startTimeMeridian = daysAndTimesWithBothMeridians[2];
        String endTimeString = daysAndTimesWithBothMeridians[3];
        String endTimeMeridian = daysAndTimesWithBothMeridians[4];

        DaysTimesCommitment daysTimesCommitment = new DaysTimesCommitment(days, startTimeString, startTimeMeridian, endTimeString,
                endTimeMeridian, commitmentType);
        
        Color currentColor = Color.BLUE;
        for(int colorIndex = 1; colorIndex <= colorNumber; colorIndex++)
        {
            currentColor = colorProducer.next();
        }
                
        daysTimesCommitment.setColor(currentColor); 
        
        return daysTimesCommitment;
    }
    
    
    public static void showSchedule(List<DaysTimesCommitment> daysTimesCommitmentList, int numberOfDaysShown, boolean isBorders, int dayWidth, int hourHeight) throws Exception {
        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                JFrame frame = new JFrame("Block Schedule");
                frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

                CalendarSettings settings = new CalendarSettings();

                settings.setMillisPerTimeslot(5 * 60 * 1000L);

                settings.setTimeslotsPerDay(15 * 12); // 12 5 minute time slots per hour.

                settings.setNumberOfDays(numberOfDaysShown);

                //DateTime(int year, int monthOfYear, int dayOfMonth, int hourOfDay, int minuteOfHour, int secondOfMinute, int millisOfSecond)
                settings.setStartDate(new DateTime(2018, 10, 1, 8, 0, 0, 0));
                TimeFinderPlanner planner = new TimeFinderPlanner(settings, dayWidth, hourHeight);

                DaysTimesCommitment daysTimesCommitmentCopy = null;
                
                try
                {
                    for (DaysTimesCommitment daysTimesCommitment : daysTimesCommitmentList) {
                        
                        daysTimesCommitmentCopy = daysTimesCommitment;

                        String days = daysTimesCommitment.daysString();

                        for (char day : days.toCharArray()) {
                            int dayIndex = 0;

                            if (day == 'M') {
                                dayIndex = 1;
                            } else if (day == 'T') {
                                dayIndex = 2;
                            } else if (day == 'W') {
                                dayIndex = 3;
                            } else if (day == 'R') {
                                dayIndex = 4;
                            } else if (day == 'F') {
                                dayIndex = 5;
                            } else if (day == 'S') {
                                dayIndex = 6;
                            } else if (day == 'U') {
                                dayIndex = 7;
                            }

                            if (dayIndex != 0) { 
                                //System.out.println(daysTimesRoom.toString()+ ", " + daysTimesRoom.startTimeHours() + ", " +  daysTimesRoom.startTimeMinutesIntoHour()+ ", " +  daysTimesRoom.durationMinutes());
                                // String description, int year, int month, int day, int hour, int minute, int durationInMinutes
                                IntervalLong interval = new IntervalLongImpl(daysTimesCommitment.toString(), 2018, 10, dayIndex, daysTimesCommitment.startTimeHours(), daysTimesCommitment.startTimeMinutesIntoHour(), daysTimesCommitment.durationMinutes());
                                interval.setColor(daysTimesCommitment.getColor());
                                interval.setIsBorder(isBorders);
                                planner.addInterval(interval);
                            }
                        }
                    }
                }
                catch (Exception e)
                {

                    JOptionPane.showMessageDialog(null, "Error with date: \n" + daysTimesCommitmentCopy.toString() + ", " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                    throw e;
                }
                
                JMenu fileMenu = new JMenu("File");
                JMenuItem saveAsImageAction = new JMenuItem();
                saveAsImageAction.setText("Save As Image");
                saveAsImageAction.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent ae) {
                        ScheduleMaker.saveImageOfComponent(planner);
                    }
                });

                fileMenu.add(saveAsImageAction);
                JMenuBar menuBar = new JMenuBar();
                menuBar.add(fileMenu);
                frame.setJMenuBar(menuBar);

                frame.setContentPane(planner);
                frame.setSize(1000, 640);
                frame.setVisible(true);
            }
        }
        );

    }

     public static void saveImageOfComponent(JComponent component) {
        JFileChooser saveImageFileChooser = new JFileChooser();

        int returnValue = saveImageFileChooser.showSaveDialog(component);

        if (returnValue == JFileChooser.APPROVE_OPTION) {
            File exportImageFile = saveImageFileChooser.getSelectedFile();
            try {
                ScreenCapture.createImage(component, exportImageFile.getAbsolutePath());
            } catch (java.io.IOException ioe) {
                ioe.printStackTrace();
            }//end try/catch.

        }
    }
     
    private void readSchedule() throws Exception
    {
        int lineNumber = 0;
        
        try ( BufferedReader br = new BufferedReader(new FileReader(scheduleFile)))
        {
            List<String> daysAndTimesList = new ArrayList();

            daysTimesCommitmentList = new ArrayList<DaysTimesCommitment>();

            String line;
            while ((line = br.readLine()) != null)
            {
                daysAndTimesList.add(line);
            }

            br.close();
            
            //HashMap<String, Color> CommitmentColorMap = new HashMap<String, Color>();
            //ColorProducer colors = new ColorProducer();
            
            for (String dayAndTime : daysAndTimesList)
            {
                lineNumber++;
                
                DaysTimesCommitment daysTimesCommitment = ScheduleMaker.createDaysTimesCommitment(dayAndTime);


                /*if(CommitmentColorMap.containsKey(daysTimesCommitment.getCommitmentType().toLowerCase()))
                {
                    daysTimesCommitment.setColor(CommitmentColorMap.get(daysTimesCommitment.getCommitmentType().toLowerCase()));
                } 
                else
                {
                    CommitmentColorMap.put(daysTimesCommitment.getCommitmentType().toLowerCase(), colors.next());
                    daysTimesCommitment.setColor(CommitmentColorMap.get(daysTimesCommitment.getCommitmentType().toLowerCase()));
                }*/
                
                
                daysTimesCommitmentList.add(daysTimesCommitment);
            }
        }
        catch (Exception e)
        {
            JOptionPane.showMessageDialog(null, "Error on line " + lineNumber, "Error", JOptionPane.ERROR_MESSAGE);
            throw e;
        }
    }
     
    private void showFrame()
    {
        JFrame frame = new JFrame();
        Container contentPane = frame.getContentPane();
        contentPane.setLayout(new java.awt.BorderLayout());

        JMenuBar menuBar = new JMenuBar();

        // ============================== File Menu
        JMenu fileMenu = new JMenu("File");

        JMenuItem openMenuItem = new JMenuItem();
        openMenuItem.setText("Open");
        openMenuItem.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent ae)
            {

                JFileChooser openFileChooser = new JFileChooser();

                //File file = new File("/home/tkosan/circuitpiper/problem_2_26.txt");// todo:tk
                //openCircuitFileChooser.setSelectedFile(file);
                FileFilter filter = new FileNameExtensionFilter("Schedule file", "txt");
                openFileChooser.addChoosableFileFilter(filter);

                int returnValue = openFileChooser.showOpenDialog(null);

                if (returnValue == JFileChooser.APPROVE_OPTION)
                {
                    scheduleFile = openFileChooser.getSelectedFile();
                    ScheduleMaker.this.fileNameField.setText(scheduleFile.getAbsolutePath());
                }
            }
        });
        
        fileMenu.add(openMenuItem);

        menuBar.add(fileMenu);

        frame.setJMenuBar(menuBar);
        
        Box box = Box.createVerticalBox();
        
        JPanel horizontalPanel = new JPanel();
        JLabel fileNameLabel = new JLabel("File");
        horizontalPanel.add(fileNameLabel);
        horizontalPanel.add(this.fileNameField);
        box.add(horizontalPanel);
        
        horizontalPanel = new JPanel();
        JLabel numberOfDaysLabel = new JLabel("Number of days shown");
        horizontalPanel.add(numberOfDaysLabel);
        numberOfdaysShownField.setText(this.numberOfDaysShown + "");
        horizontalPanel.add(this.numberOfdaysShownField);
        box.add(horizontalPanel);
        
        horizontalPanel = new JPanel();
        JLabel dayWidthLabel = new JLabel("Day width");
        horizontalPanel.add(dayWidthLabel);
        daysWidthField.setText(this.daysWidth + "");
        horizontalPanel.add(this.daysWidthField);
        box.add(horizontalPanel);
        
        horizontalPanel = new JPanel();
        JLabel hourHeightLabel = new JLabel("Hour height");
        horizontalPanel.add(hourHeightLabel);
        hoursHeightField.setText(this.hoursHeight + "");
        horizontalPanel.add(this.hoursHeightField);
        box.add(horizontalPanel);
        
        this.fileNameField.setEditable(false);
        
        JButton viewScheduleButton = new JButton("View Schedule");
        viewScheduleButton.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent ae)
            {
                if (scheduleFile == null)
                {
                    JOptionPane.showMessageDialog(null, "No file has been selected.", "Error", JOptionPane.ERROR_MESSAGE);
                }
                else
                {
                    SwingUtilities.invokeLater(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            try
                            {
                                ScheduleMaker.this.readSchedule();

                                ScheduleMaker.this.numberOfDaysShown = Integer.parseInt(ScheduleMaker.this.numberOfdaysShownField.getText());
                                ScheduleMaker.this.daysWidth = Integer.parseInt(ScheduleMaker.this.daysWidthField.getText());
                                ScheduleMaker.this.hoursHeight = Integer.parseInt(ScheduleMaker.this.hoursHeightField.getText());

                                ScheduleMaker.showSchedule(daysTimesCommitmentList, ScheduleMaker.this.numberOfDaysShown, isBorders, ScheduleMaker.this.daysWidth, ScheduleMaker.this.hoursHeight);
                            }
                            catch (Exception e)
                            {
                                e.printStackTrace();
                            }
                        }

                    });
                }
            }
        });
        
        box.add(viewScheduleButton);
        
        
        JPanel jPanel = new JPanel();
        jPanel.add(box);
        contentPane.add(jPanel);
        
        //frame.setAlwaysOnTop(false);
        frame.setTitle("Schedule Viewer v.07");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        int height = screenSize.height;
        int width = screenSize.width;
        frame.setSize(width / 2, height / 2);
        frame.setResizable(true);

        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }
    
    
    public static void main(String[] args) throws Exception
    {
        
        /*List<String> daysAndTimesStrings = new ArrayList(Arrays.asList("TR 4:10 - 5:10 PM \"more work\""));
        
        List<DaysTimesCommitment> daysTimesCommitmentList = new ArrayList<DaysTimesCommitment>();
        
        Boolean isBorders = true;
        int dayWidth = 200;
        int hourHeight = 100;
        
        ColorProducer colors = new ColorProducer();
        
        for()
        {
            
        }
        
        for(String dayAndTime: daysAndTimesStrings)
        {
            DaysTimesCommitment daysTimesCommitment = ScheduleMaker.createDaysTimesCommitment(dayAndTime);
            
            daysTimesCommitment.setColor(Color.yellow);
                            
            daysTimesCommitmentList.add(daysTimesCommitment);
        }
        
        ScheduleMaker.showSchedule(daysTimesCommitmentList, 7, isBorders, dayWidth, hourHeight);*/
         

        
        ScheduleMaker scheduleMaker = new ScheduleMaker();
        
        scheduleMaker.showFrame();
    }
}
